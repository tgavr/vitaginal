<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);


Route::get('/instruction', 'HomeController@instruction');
Route::get('/reviews', 'HomeController@reviews');

Route::get('/', 'HomeController@index');

Route::get('/about', 'HomeController@about');
// Route::get('/about_test', 'HomeController@about_test');
Route::get('/agreement', 'HomeController@agreement');
Route::get('/conf', 'HomeController@conf');
Route::get('/articles', 'HomeController@articles');

Route::get('/hochu-stat-mamoj-5-kriteriev-uspekha', 'HomeController@stage1');
Route::get('/zhdem-malysha-5-prichin-schastlivoj-beremennosti', 'HomeController@stage2');
Route::get('/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha', 'HomeController@stage3');


Route::get('/articles/{slug}', 'HomeController@article');

Route::get('/become-mom-calculator', 'HomeController@calcBecomeMom');
Route::get('/calculator_pregnancy', 'HomeController@calcPregnancy');
Route::get('/calculator_height-weight', 'HomeController@calcHeight');




Route::get('/home', function () {
	return redirect('/admin');
});