<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Week;
use App\ChildValue;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function instruction()
    {
        return view('instruction');
    }

    public function reviews()
    {
        $reviews = \App\Review::where('is_published', 1)->get();

        return view('reviews', ['reviews' => $reviews]);
    }

    public function agreement()
    {
        return view('agreement');
    }

    public function conf()
    {
        return view('conf');
    }

    public function calcBecomeMom(Request $request)
    {

        $articles = \App\Article::where('on_stage1', true)->get();

        return view('calc_become_mom', ['articles' => $articles]);
    }

    public function calcPregnancy(Request $request)
    {
        $date_type = $request->input('date_type');
        $date = $request->input('date');
        $pdr = '';
        $state = '';

        $date_d = \DateTime::createFromFormat("d.m.Y", $date);

        if (!$date) {
            $week = 0;
        } elseif ($date_d->format("Y-m-d") > date("Y-m-d")) {
            
            $week = 0;
            $state = "Ошибочное значение!";

        } elseif ($date_type == "date_last_cile") {
            $date_now = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));
            $ddd = $date_now->diff($date_d)->days;

            $pdr = $date_d->add(date_interval_create_from_date_string('280 days'))->format("d.m.Y");

            $date_d = \DateTime::createFromFormat("d.m.Y", $date);
            $week = ceil($ddd/7);

            if ($week < 1) $state = "Ошибочное занчение";
            if ($week > 40) $state = "Пора в роддом!";

            $week = Week::where('num', $week)->first();

        } elseif ($date_type == "date_conception") {

            $pdr = $date_d->add(date_interval_create_from_date_string('264 days'))->format("d.m.Y");

            $date_d = \DateTime::createFromFormat("d.m.Y", $date);

            $date_now = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));
            $ddd = $date_now->diff($date_d)->days;
            $week = ceil($ddd/7);

            $week = $week+2;

            if ($week < 1) $state = "Ошибочное занчение";
            if ($week > 40) $state = "Пора в роддом!";

            $week = Week::where('num', $week)->first();

        }

        if ($request->input('week')) {

            $week = $request->input('week');
            $week = Week::where('num', $week)->first();
        }

        $articles = \App\Article::where('on_stage2', true)->get();

        return view('calc_pregnancy', ['week' => $week, 'date_type' => $date_type, 'date' => $date, 'pdr' => $pdr, 'state' => $state, 'articles' => $articles]);
    }

    public function calcHeight(Request $request)
    {

        $bird_date = $request->input('bird_date');
        $bird_date_t = \DateTime::createFromFormat("d.m.Y", $bird_date);

        $weight = $request->input('weight');
        $height = $request->input('height');


        $values = [
                'weight' => [
                    'state' => 'в норме',
                    'state2' => 'Средний',
                    'color' => 'green'
                ],
                'height' => [
                    'state' => 'в норме',
                    'state2' => 'Средний',
                    'color' => 'green'
                ],
                'imt' => [
                    'state' => 'в норме',
                    'state2' => 'Нормальная масса тела',
                    'color' => 'green'
                ]
            ];

        $y = ""; $d = ""; $m = ""; $word = ""; $gender = "";

        if (!$bird_date_t) {
            
        } else {

            $date_now = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));
            $interval = $date_now->diff($bird_date_t);

            $d = $interval->d;
            $m = $interval->m;
            $y = $interval->y;

            $mounths = $y*12 + $m;

            if ($d > 9 && $d < 20) {
                $d = "$d дней";
            } else {
                if (in_array($d % 10, [0, 5, 6, 7, 8, 9])) {
                    $d = "$d дней";
                } elseif (in_array($d % 10, [1])) {
                    $d = "$d день";
                } elseif (in_array($d % 10, [2, 3, 4])) {
                    $d = "$d дня";
                }
            }

            if ($m > 9 && $m < 20) {
                $m = "$m месяцев";
            } else {
                if (in_array($m % 10, [0, 5, 6, 7, 8, 9])) {
                    $m = "$m месяцев";
                } elseif (in_array($m % 10, [1])) {
                    $m = "$m месяц";
                } elseif (in_array($m % 10, [2, 3, 4])) {
                    $m = "$m месяца";
                }
            }

            if ($y > 9 && $y < 20) {
                $y = "$y лет";
            } else {
                if (in_array($y % 10, [0, 5, 6, 7, 8, 9])) {
                    $y = "$y лет";
                } elseif (in_array($y % 10, [1])) {
                    $y = "$y год";
                } elseif (in_array($y % 10, [2, 3, 4])) {
                    $y = "$y года";
                }
            }

            $word = $request->input('gender') == 'girl' ? 'Девочке' : 'Мальчику';
            $gender = $request->input('gender');

            $height = $request->input('height');
            $weight = $request->input('weight');

            $child_value = ChildValue::where('gender', $gender)->where('mounths', '>=', $mounths)->orderBy('id', 'asc')->first();
            if (!$child_value) $child_value = ChildValue::where('gender', $gender)->orderBy('id', 'desc')->first();

            if ($weight < $child_value->weight_min) {

                $values['weight']['state'] = 'ниже нормы';
                $values['weight']['state2'] = 'Истощение';
                $values['weight']['color'] = 'red';

            } elseif ($weight < $child_value->weight_min + $child_value->weight_min * 0.04) {
                
                $values['weight']['state'] = 'ниже нормы';
                $values['weight']['state2'] = 'Истощение';
                $values['weight']['color'] = 'yellow';

            } elseif ($weight > $child_value->weight_max) { 

                $values['weight']['state'] = 'выше нормы';
                $values['weight']['state2'] = 'Избыток';
                $values['weight']['color'] = 'red';

            } elseif ($weight > $child_value->weight_max - $child_value->weight_max * 0.04) {

                $values['weight']['state'] = 'выше нормы';
                $values['weight']['state2'] = 'Избыток';
                $values['weight']['color'] = 'yellow';
            }

            if ($height < $child_value->height_min) {

                $values['height']['state'] = 'ниже нормы';
                $values['height']['state2'] = 'Очень низкий';
                $values['height']['color'] = 'red';

            } elseif ($height < $child_value->height_min + $child_value->height_min * 0.04) {
                
                $values['height']['state'] = 'ниже нормы';
                $values['height']['state2'] = 'Низкий';
                $values['height']['color'] = 'yellow';

            } elseif ($height > $child_value->height_max) { 

                $values['height']['state'] = 'выше нормы';
                $values['height']['state2'] = 'Очень высокий';
                $values['height']['color'] = 'red';

            } elseif ($height > $child_value->height_max - $child_value->height_max * 0.04) {

                $values['height']['state'] = 'выше нормы';
                $values['height']['state2'] = 'Высокий';
                $values['height']['color'] = 'yellow';
            }

            $imt = ($weight/($height*$height))*100*100;

            if ($imt > $child_value->imt2) {

                $values['imt']['state'] = 'выше нормы';
                $values['imt']['state2'] = 'Избыточная масса тела';
                $values['imt']['color'] = 'red';
                $values['imt']['value'] = round($imt, 2);
                $values['imt']['min'] = $child_value->imt1;
                $values['imt']['max'] = $child_value->imt2;

                $values['imt']['text'] = ' Избыточная масса тела. Для корректировки рациона и получения рекомендаций обратитесь к врачу.';

            } elseif ($imt < $child_value->imt1) {

                $values['imt']['state'] = 'ниже нормы';
                $values['imt']['state2'] = 'Истощение';
                $values['imt']['color'] = 'red';
                $values['imt']['value'] = round($imt, 2);
                $values['imt']['min'] = $child_value->imt1;
                $values['imt']['max'] = $child_value->imt2;

                $values['imt']['text'] = 'Дефицит массы тела. Для корректировки рациона и получения рекомендаций обратитесь к врачу.';

            } else {
                $values['imt']['state'] = 'в норме';
                $values['imt']['state2'] = 'Норма';
                $values['imt']['color'] = 'green';
                $values['imt']['value'] = round($imt, 2);
                $values['imt']['min'] = $child_value->imt1;
                $values['imt']['max'] = $child_value->imt2;

                $values['imt']['text'] = '';
            }
        }

         $articles = \App\Article::where('on_stage3', true)->get();

         $page = \App\Page::where('url', 'calc_height')->first();

        return view('calc_height', ['d' => $d, 'm' => $m, 'y' => $y, 'word' => $word, 'gender' => $gender, 'values' => $values, 'weight' => $weight, 'height' => $height, 'bird_date' => $bird_date, 'articles' => $articles, 'page' => $page]);
    }


    public function index()
    {
        $articles = \App\Article::where('on_main', true)->get();
        $pharmacies = \App\Pharmacy::get();
        $page = \App\Page::where('url', 'main')->first();

        return view('index', ['ddd' => 1, 'page' => $page, 'articles' => $articles, 'pharmacies' => $pharmacies]);
    }

    public function about()
    {
        $pharmacies = \App\Pharmacy::get();
        $page = \App\Page::where('url', 'main')->first();

        return view('about', ['page' => $page, 'pharmacies' => $pharmacies]);
    }

    public function about_test()
    {
        $articles = \App\Article::where('on_main', true)->get();
        $pharmacies = \App\Pharmacy::get();
        $page = \App\Page::where('url', 'main')->first();

        return view('index_test', ['ddd' => 1, 'page' => $page, 'articles' => $articles, 'pharmacies' => $pharmacies]);
    }

    public function stage1()
    {
        $articles = \App\Article::where('on_stage1', true)->get();
        $page = \App\Page::where('url', 'stage1')->first();

        return view('stage1', ['page' => $page, 'articles' => $articles]);
    }

    public function stage2()
    {
        $articles = \App\Article::where('on_stage2', true)->get();
        $page = \App\Page::where('url', 'stage2')->first();

        return view('stage2', ['page' => $page, 'articles' => $articles]);
    }

    public function stage3()
    {
        $articles = \App\Article::where('on_stage3', true)->get();
        $page = \App\Page::where('url', 'stage3')->first();

        return view('stage3', ['page' => $page, 'articles' => $articles]);
    }

    public function stage1_test()
    {
        $articles = \App\Article::where('on_stage1', true)->get();
        $page = \App\Page::where('url', 'stage1')->first();

        return view('stage1_test', ['page' => $page, 'articles' => $articles]);
    }

    public function stage2_test()
    {
        $articles = \App\Article::where('on_stage2', true)->get();
        $page = \App\Page::where('url', 'stage2')->first();

        return view('stage2_test', ['page' => $page, 'articles' => $articles]);
    }

    public function stage3_test()
    {
        $articles = \App\Article::where('on_stage3', true)->get();
        $page = \App\Page::where('url', 'stage3')->first();

        return view('stage3_test', ['page' => $page, 'articles' => $articles]);
    }

    public function articles(Request $request)
    {
        $stage = $request->input('stage');

        $articles = \App\Article::orderBy('id');

        if ($stage == 1) {
            $articles = $articles->where('on_stage1', true);
        } elseif ($stage == 2) {
            $articles = $articles->where('on_stage2', true);
        } elseif ($stage == 3) {
            $articles = $articles->where('on_stage3', true);
        } else {
            $stage = 0;
        }

        $articles = $articles->get();

        return view('articles', ['stage' => $stage, 'articles' => $articles]);
    }

    public function article($slug)
    {
        $article = \App\Article::where('url', $slug)->first();

        if (!$article) {
            return abort(404);
        }

        return view('article', ['article' => $article]);
    }
}
