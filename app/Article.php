<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = [
		'articles'
	];

    public function articles()
	{
		return $this->belongsToMany('\App\Article', 'article_articles', 'artcle_main_id', 'artcle_id');
	}

	public function setArticlesAttribute($articles)
	{
		$this->articles()->detach();
		if ( ! $articles) return;

		if ( ! $this->exists) $this->save();

		$this->articles()->attach($articles);
	}
}
