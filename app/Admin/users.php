<?php
use App\User;
use App\Client;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(User::class, function (ModelConfiguration $model) {
    $model->setTitle('Пользователи');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            // ->setDisplaySearch(true)
            ->setColumns([
                AdminColumn::checkbox(),
                AdminColumn::link('id')->setLabel('ID')->setWidth('50px'),
                AdminColumn::text('name')->setLabel('Имя')->setWidth('200px'),
                AdminColumn::text('email')->setLabel('Мэйл')->setWidth('200px')
               
            
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('name', 'Название')->unique(),
            AdminFormElement::text('email', 'email')->unique(),
            AdminFormElement::password('password', 'Пароль')->hashWithBcrypt()
        );
        return $form;
    });
}); 