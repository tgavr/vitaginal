<?php
use App\Week;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Week::class, function (ModelConfiguration $model) {
    $model->setTitle('Недели');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            ->setColumns([
                AdminColumn::text('num')->setLabel('Номер')->setWidth('200px'),
                AdminColumn::text('fruit')->setLabel('Фрукт')->setWidth('200px')
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::number('num', 'Номер'),
            AdminFormElement::text('fruit', 'Фрукт'),
            AdminFormElement::image('fruit_img', 'Картинка фрукта'),
            AdminFormElement::wysiwyg('text1', 'Текст 1'),
            AdminFormElement::wysiwyg('text2', 'Текст 2'),
            AdminFormElement::wysiwyg('text3', 'Текст 3')
        );
        return $form;
    });
}); 