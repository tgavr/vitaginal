<?php

use SleepingOwl\Admin\Navigation\Page;

return [
    [
        'title' => 'Страницы',
        'icon'  => 'fas fa-tachometer-alt',
        'url'   => '/admin/pages'
    ],

    [
        'title' => 'Статьи',
        'icon'  => 'fas fa-info-circle',
        'url'   => '/admin/articles'
    ],

    [
        'title' => 'Аптеки',
        'icon'  => 'fas fa-info-circle',
        'url'   => '/admin/pharmacies'
    ],

    [
        'title' => 'Пользователи',
        'icon'  => 'fas fa-info-circle',
        'url'   => '/admin/users'
    ],

    [
        'title' => 'Недели',
        'icon'  => 'fas fa-info-circle',
        'url'   => '/admin/weeks'
    ],[
        'title' => 'Отзывы',
        'icon'  => 'fas fa-info-circle',
        'url'   => '/admin/reviews'
    ]
    
];
