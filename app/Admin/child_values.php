<?php
use App\ChildValue;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(ChildValue::class, function (ModelConfiguration $model) {
    $model->setTitle('Статья');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            ->setColumns([
                AdminColumn::text('gender')->setLabel('Пол')->setWidth('200px'),
                AdminColumn::text('mounths')->setLabel('Месяц')->setWidth('200px'),
                AdminColumn::text('height_min')->setLabel('Минимальный рост')->setWidth('200px'),
                AdminColumn::text('height_max')->setLabel('Максимальный рост')->setWidth('200px'),
                AdminColumn::text('weight_min')->setLabel('Минимальный вес')->setWidth('200px'),
                AdminColumn::text('weight_max')->setLabel('Максимальный вес')->setWidth('200px'),
                AdminColumnEditable::text('imt1')->setLabel('ИМТ избыток')->setWidth('200px'),
                AdminColumnEditable::text('imt2')->setLabel('ИМТ ожирение')->setWidth('200px')
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('gender', 'Пол'),
            AdminFormElement::text('mounths', 'Месяц'),
            AdminFormElement::text('height_min', 'Минимальный рост'),
            AdminFormElement::text('height_max', 'Максимальный рост'),
            AdminFormElement::text('weight_min', 'Минимальный вес'),
            AdminFormElement::text('weight_max', 'Максимальный вес'),

            AdminFormElement::text('imt1', 'ИМТ избыток'),
            AdminFormElement::text('imt2', 'ИМТ ожирение')
        );
        return $form;
    });

    // * Прикрепленные статьи
}); 