<?php
use App\Page;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Page::class, function (ModelConfiguration $model) {
    $model->setTitle('Страницы');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            ->setColumns([
                AdminColumn::text('url')->setLabel('URL')->setWidth('200px'),
                AdminColumn::text('title')->setLabel('Title')->setWidth('200px')
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('url', 'URL'),
            AdminFormElement::text('title', 'Title'),
            AdminFormElement::textarea('description', 'description'),
            AdminFormElement::wysiwyg('literature', 'Список литературы')
        );
        return $form;
    });
}); 