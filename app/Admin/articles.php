<?php
use App\Article;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Article::class, function (ModelConfiguration $model) {
    $model->setTitle('Статья');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            ->setColumns([
                AdminColumn::text('title')->setLabel('Заголовок')->setWidth('200px'),
                AdminColumn::image('banner')->setLabel('Баннер')->setWidth('200px')
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {

        $articles = Article::get();
        $articles_list = [];

        foreach ($articles as $article) {
            $articles_list[$article->id] = $article->title;
        }

        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('url', 'URL'),
            AdminFormElement::text('title', 'Title'),
            AdminFormElement::wysiwyg('text', 'Текст'),
            AdminFormElement::textarea('tiser', 'Тизер'),

            AdminFormElement::textarea('description', 'Description'),
            AdminFormElement::image('banner', 'Баннер'),
            AdminFormElement::image('small_banner', 'Мини-Баннер'),

            AdminFormElement::checkbox('on_main', 'На главной'),
            AdminFormElement::checkbox('on_stage1', 'Хочу стать мамой'),
            AdminFormElement::checkbox('on_stage2', 'Ждем малыша'),
            AdminFormElement::checkbox('on_stage3', 'Мама, я родился'),

            AdminFormElement::wysiwyg('literature', 'Список литературы'),

            AdminFormElement::file('link', 'Ссылка'),
            AdminFormElement::text('link_text', 'Текст ссылки'),

            AdminFormElement::multiselect('articles', 'Статьи', $articles_list)
        );
        return $form;
    });

    // * Прикрепленные статьи
}); 