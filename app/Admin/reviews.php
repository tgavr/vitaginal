<?php
use App\Review;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Review::class, function (ModelConfiguration $model) {
    $model->setTitle('Отзывы');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            ->setColumns([
                AdminColumn::text('created_at')->setLabel('Отзыв добавлен')->setWidth('200px'),
                AdminColumn::text('name')->setLabel('Имя')->setWidth('200px'),
                AdminColumn::text('stars')->setLabel('Оценка')->setWidth('200px'),
                AdminColumn::text('text')->setLabel('Текст отзыва')->setWidth('200px'),
                AdminColumnEditable::checkbox('is_published')->setLabel('Публиковать')->setWidth('200px')
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {

        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('name', 'Имя'),
            AdminFormElement::text('stars', 'Оценка'),
            AdminFormElement::text('text', 'Текст'),
            AdminFormElement::text('link', 'Ссылка'),
            AdminFormElement::text('site', 'Название площадки'),
            AdminFormElement::checkbox('is_published', 'Публиковать')
        );
        return $form;
    });

    // * Прикрепленные статьи
}); 
// 52c8f5cc-30e2-44c3-878a-62828bf85591