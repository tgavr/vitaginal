<?php
use App\Pharmacy;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Pharmacy::class, function (ModelConfiguration $model) {
    $model->setTitle('Аптеки');
    // Display
    $model->onDisplay(function () {

        $display = AdminDisplay::datatablesAsync();
        $display
            // ->setDisplaySearch(true)
            ->setColumns([
                AdminColumn::checkbox(),
                AdminColumn::image('logo')->setLabel('Лого')->setWidth('200px'),
                AdminColumn::text('link')->setLabel('Ссылка')->setWidth('200px')
               
            
            ])
        ;


        $display->paginate(15);
        return $display;
    });

    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::image('logo', 'Логотип'),
            AdminFormElement::text('link', 'Ссылка')
        );
        return $form;
    });
}); 