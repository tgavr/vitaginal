<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use App\CrmApi as Crm;
use App\Dealer;
use App\Review;
use App\Lead;
use App\GoogleBearerToken;
use App\GoogleSpreadSheetApi as GSA;

class GetReviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get_reviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $i = 0;
        $u = true;
        $ii = 0;

        while ($ii < 20) {

            $ii = $i*20;
            $id = "52c8f5cc-30e2-44c3-878a-62828bf85591";
            $url = "https://api.mneniya.pro/v2.1/clients/52c8f5cc-30e2-44c3-878a-62828bf85591/reviews/product/Aggregated/001?count=20&offset=$ii";

            $json = file_get_contents($url);
            $rows = json_decode($json)->Reviews;

            if (!count($rows)) $u = false;
            foreach ($rows as $row) {
                var_dump($row);

                $r = Review::where('external_id', $row->ID)->first();
                sleep(2);

                if ($r) continue;

                $r = new Review;

                $r->external_id = $row->ID;
                $r->name = $row->AuthorName;
                $r->stars = $row->Rate;
                $r->text = $row->ReviewText;
                $r->link = $row->SourceURL;
                
                $r->is_published = 0;

                $site = $r->link;
                $site = str_replace("https://", "", $site);
                $site = str_replace("http://", "", $site);
                $site = explode("/", $site)[0];
                $r->site = $site;

                $r->save();
            }

            var_dump($url);

            $i++;

            sleep(2);
        }
    }
}
