if ($(window).width() < 768) {
  $('.home-screen [data-parallax]').attr("data-parallax", '{"y": 0}');
  $('.importance-block__img [data-parallax]').attr("data-parallax", '{"y": 0}');
}

if ($('[data-parallax]').length) {
  $('[data-parallax]').parallax({
    "enableTouchDevices": true,
  })
  
}

$(function () {
  var OPTIONS = {};
  main(OPTIONS);


});

function main(opts) {
  // $("body").mCustomScrollbar({
  //   mouseWheel: { scrollAmount: '500' }
  // });
  
  headerPosition();
  homeHeroSlider();
  homeAboutSlider();
  homeScreenSlider();
  accoFn();
  scrollToFunctionality();
  usefulArticlesSlider();
  boxAnimation();
  parallaxInit();
  parallaxInit();
  momBabySlider();
  sliderPopupSwiper();
  utekaWidjet();
  inputMask();
  calendarForm();

  calculatorCommonFn();
  calculatorOvulation();
  calcUlatorWeightHeight();
  calculatorPregnacy();

  closeOuterBlockListener($('.js-dropdown'), function($block) {
    $block.removeClass("dropdown--open");
  });

  $(document).on("click", ".js-toggle-dropdown", function(e) {
    e.preventDefault();
    $(this).closest('.js-dropdown').toggleClass("dropdown--open");
  }).on("click", ".js-mob-menu-btn", function(e) {
    e.preventDefault();
    $(this).toggleClass("active");
    $('.header').removeClass('header--refresh-transition').toggleClass("menu-open");
    $('.mob-menu').toggleClass("open");
  }).on("click", ".js-show-all", function(e) {
    e.preventDefault();
    var attr = $(this).attr('data-target');
    if(attr) {
      $(attr).toggleClass('show-all');
      $(this).toggleClass('btn--toggle');
    }
  });

  $(document).on("click touchstart", ".play", function(e) {
    e.preventDefault();
    videoSrc = $(this).attr("data-video-src");
    $(this).closest(".video-wrap").find('.video').attr('src', ''+videoSrc+'?autoplay=1&rel=0').addClass("visible");
  });

  $(".footer__сookie .btn").click(function(){
    $(this).closest(".footer__сookie").fadeOut();
  });

  $(".footer__scroll-top").click(function(){
    $('html, body').stop().animate({
      scrollTop: 0
    }, 1000);
  });

  $('.counter-el').each(function(_, el){
    var $counterEl = $(el);
    var $control = $counterEl.find('.counter-el__control');
    var min = +$control.attr('data-min');
    var max = +$control.attr('data-max');

    $control.val(min);

    $counterEl.on('click', '.counter-el__btn--decr', function(e) {
      e.preventDefault();
      $control.val(+$control.val() - 1);

      if(!isNaN(Number(min))) {
        +$control.val() < min && $control.val(min);
      } else if(!$control.val()){
        $control.val(0);
      } else {
        $control.trigger('change');
      }
    }).on('click', '.counter-el__btn--incr', function(e) {
      e.preventDefault();
      $control.val(+$control.val() + 1);

      if(!isNaN(Number(max))) {
        +$control.val() >= max && $control.val(max);
      } else {
        $control.trigger('change');
      }
    });

    $control.on('input', function(e) {
      var res = e.target.value.replace(/\D/, '');
      this.value = res;
    });
  });


  $(".reviews-block__btn").click(function () {
    $(".reviews-block__col.hide").removeClass("hide");
  })

}

function reflow(element) {
  return element.offsetHeight;
}

function headerPosition() {
  var header = $('.header');
  var headerPos = function () {
    if($(window).scrollTop() > 0) {
      header.removeClass('header--refresh-transition').addClass('header--scrolled');
    } else {
      header.addClass('header--refresh-transition').removeClass('header--scrolled');
    }
  };
  
  headerPos();
  
  $(window).on('scroll', headerPos);
}

function homeHeroSlider() {
  var ctx = document.querySelector('.hero-slider .swiper-container');

  if(ctx) {
    var swiper = new Swiper(ctx, {
      slidesPerView: 1,
      speed: 1000,
      // autoplay: {
      //   delay: 2000,
      //   disableOnInteraction: false
      // },
      loop: true,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      allowTouchMove: false,
      on: {
        slideChange: function() {
          $('.home-scroll-btn').removeClass('active').eq(this.realIndex).addClass('active');
        }
      }
    });

    swiper.slideToLoop(1, 0, false);
    swiper.update();
  }
}

function usefulArticlesSlider() {
  $('.useful-articles-slider').each(function(){
    var usefulArticles = $(this);
    var usefulArticlesCntr = usefulArticles.closest('.useful-articles');
    var nextEl = usefulArticles.find('.swiper-button-next');
    var prevEl = usefulArticles.find('.swiper-button-prev');
    var usefulArticlesSlider;
    var spaceBetween = 32;
    var spaceBetweenMob = 12;

    if (usefulArticles.hasClass("space-between-20-10")) {
      spaceBetween = 20;
      spaceBetweenMob = 10;
    }

    if(usefulArticles.length) {
      usefulArticlesSlider = new Swiper(usefulArticles.find('.swiper-container')[0], {
        slidesPerView: "auto",
        spaceBetween: spaceBetweenMob,
        navigation: {
          nextEl: nextEl[0],
          prevEl: prevEl[0],
        },
        breakpoints: {
          767.98: {
            spaceBetween: spaceBetween,
          }
        },
        on: {
          init: function() {
            usefulArticles.removeClass('useful-articles-slider--ended').addClass('useful-articles-slider--started');
            usefulArticlesCntr.removeClass('useful-articles--ended').addClass('useful-articles--started');
          },
          reachBeginning: function(e) {
            usefulArticles.addClass('useful-articles-slider--started');
            usefulArticlesCntr.addClass('useful-articles--started');
          },
          reachEnd: function(e) {
            usefulArticles.addClass('useful-articles-slider--ended');
            usefulArticlesCntr.addClass('useful-articles--ended');
          },
          fromEdge: function(e) {
            usefulArticles.removeClass('useful-articles-slider--ended').removeClass('useful-articles-slider--started');
            usefulArticlesCntr.removeClass('useful-articles--ended').removeClass('useful-articles--started');
          }
        }
      });
    }
  });
}

function homeAboutSlider() {
  var ctx = document.querySelector('.home-about-tooltips .swiper-container');
  var ctxPagination = document.querySelector('.home-about-tooltip-pgn');

  if(ctx && ctxPagination) {
    new Swiper(ctx, {
      slidesPerView: 1,
      autoplay: {
        delay: 4000,
        // disableOnInteraction: false
      },
      loop: true,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
      pagination: {
        el: ctxPagination,
        type: 'bullets',
        clickable: true,
        bulletClass: 'home-tooltip-pgn',
        bulletActiveClass: 'home-tooltip-pgn-active',
        renderBullet: function (index, className) {
          var data = this.$el[0].querySelector('[data-swiper-slide-index="' + index + '"]').querySelector('.home-about-tooltip').dataset;
          return '<span class="' + className + ' ' + className + '-' + index + '"><span class="home-tooltip-pgn__inner"><span class="home-tooltip-pgn__name">' + data.name + (data.num ? '<sub>' + data.num +'</sub>' : '') + '</span><span class="home-tooltip-pgn__small">' + data.small + '</span></span></span>';
        }
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      allowTouchMove: false
    });
  }
}

function homeScreenSlider() {
  var sliders = Array.prototype.slice.call(document.querySelectorAll('.home-screen-slider'));

  sliders.forEach(function(sl) {
    var ctx = sl.querySelector('.swiper-container');
    var $screen = $(ctx).closest('.home-screen');
    var timeout = null;

    new Swiper(ctx, {
      slidesPerView: 'auto',
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        767.98: {
          spaceBetween: 20,
        }
      },
      on: {
        init: function() {
          $screen.removeClass('home-screen--ended').addClass('home-screen--started');
        },
        reachBeginning: function(e) {
          timeout = setTimeout(function() {
            $screen.addClass('home-screen--started');
            timeout = null;
          }, 100);
        },
        reachEnd: function(e) {
          timeout = setTimeout(function() {
            $screen.addClass('home-screen--ended');
            timeout = null;
          }, 100);
        },
        fromEdge: function(e) {
          if(!timeout !== null) {
            $screen.removeClass('home-screen--ended').removeClass('home-screen--started');
          }
        }
      }
    });
  });
}

function accoFn() {
  var acco = $('.js-acco');
  var activeClass = 'active';

  acco.each(function (_, a) {
    var $current = $(a);
    $current.on('click', '.js-acco-head', accoClickHadler($current));

    function accoClickHadler($acco) {
      var $items = $acco.find('.js-acco-head');

      return function (e) {
        e.preventDefault();
        var $self = $(this);

        $items.each(function (_, i) {
          if ($(i).is($self)) {
            if ($(i).hasClass(activeClass)) {
              $(i).removeClass(activeClass).next().slideUp();
            } else {
              $(i).addClass(activeClass).next().slideDown();
            }
          } else {
            $(i).removeClass(activeClass).next().slideUp();
          }
        });
      }
    }
  });
}

function closeOuterBlockListener($block, handler) {
  $(document).on('mouseup', function (e) {
    var div = $block;
    if (!div.is(e.target) && div.has(e.target).length === 0) {
      typeof handler === 'function' && handler(div);
    }
  });
}

function scrollToFunctionality() {
  $('.scroll-to').on('click', function (e) {
    e.preventDefault();
    var scroll_el = $(this).attr('href');
    if ($(scroll_el).length !== 0) {

      // close menu
      $('.js-mob-menu-btn').removeClass("active");
      $('.header').removeClass('header--refresh-transition').removeClass("menu-open");
      $('.mob-menu').removeClass("open");
      // close menu

      $('html, body').stop().animate({
        scrollTop: $(scroll_el).offset().top - $('.header').height()
      }, 1000);
    }
    return false;
  });
}

function parallaxInit() {
	$.fn.parallax = function(resistance, mouse) {
		$el = $(this);
		TweenLite.to($el, 0.2, {
			x: -((mouse.clientX - window.innerWidth / 2) / resistance),
			y: -((mouse.clientY - window.innerHeight / 2) / resistance)
		});
	};

	$(document).on('mousemove', function(e) {
		if ($(".js-parallax-1").length) {
			$(".js-parallax-1").parallax(100, e);
		}

		if ($(".js-parallax-2").length) {
			$(".js-parallax-2").parallax(-100, e);
		}

		if ($(".js-parallax-3").length) {
			$(".js-parallax-3").parallax(20, e);
		}

		if ($(".js-parallax-4").length) {
			$(".js-parallax-4").parallax(-30, e);
		}

		if ($(".js-parallax-5").length) {
			$(".js-parallax-5").parallax(30, e);
		}

		if ($(".js-parallax-6").length) {
			$(".js-parallax-6").parallax(-40, e);
    }

    if ($(".js-parallax-7").length) {
			$(".js-parallax-7").parallax(50, e);
		}

		if ($(".js-parallax-8").length) {
			$(".js-parallax-8").parallax(40, e);
		}
	});
}

function momBabySlider() {
  var ctx = document.querySelector('.mom-baby-replaceable .swiper-container');
  var ctxPagination = document.querySelector('.mom-baby-tooltip-pgn');

  if(ctx && ctxPagination) {
    var effect = "fade";

    if ($(window).width() < 768) {
      effect = "slide";
    }

    new Swiper(ctx, {
      slidesPerView: "auto",
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      effect: effect,
      fadeEffect: {
        crossFade: true
      },

      loop: true,
      spaceBetween: 10,
      
      pagination: {
        el: ctxPagination,
        type: 'bullets',
        effect: 'slide',
        clickable: true,
        bulletClass: 'mom-baby-tooltip-pgn__item',
        bulletActiveClass: 'mom-baby-tooltip-pgn-active',
        renderBullet: function (index, className) {
          var data = this.$el[0].querySelector('[data-swiper-slide-index="' + index + '"]').querySelector('.mom-baby-tooltip').dataset;
          return '<span class="' + className + ' ' + className + '-' + index + '"><span class="mom-baby-tooltip-pgn__inner"><span class="mom-baby-tooltip-pgn__name-ico"><span class="mom-baby-tooltip-pgn__name">' + data.name + (data.num ? '<sub>' + data.num +'</sub>' : '') + '</span></span><span class="mom-baby-tooltip-pgn__small">' + data.small + '</span></span></span>';
        }
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      breakpoints: {
        575: {
          slidesPerView: 2,
        },

        767.98: {
          slidesPerView: 1,
          allowTouchMove: false,
          spaceBetween: 0,
        }
      },
      
    }); 
  }
}


function sliderPopupSwiper() {
  $(".slider-popup__swiper").each(function() {
    var $swiperEl = $(this);

    var mySwiper = new Swiper($swiperEl.find(".swiper-container"), {
      init: false,
      spaceBetween: 4,
      slidesPerView: "auto",
      navigation: {
        nextEl: $swiperEl.closest(".slider-popup").find(".slider-popup__swiper-next"),
        prevEl: $swiperEl.closest(".slider-popup").find(".slider-popup__swiper-prev"),
      }
    });

    $(".js-slider-more").click(function(e){
      e.preventDefault();
      // var thisTarget = $(this).data('index');
      var slideNum = $(this).closest("[data-swiper-slide-index]").attr("data-swiper-slide-index");
      console.log(slideNum);
      $.fancybox.open({
        src: "#slider-more",
        type: 'inline',
        opts: {
          baseClass: "fancybox-container-slider-popup",
          toolbar: false,
          defaultType: 'inline',
          autoFocus: true,
          touch: false,
          afterLoad: function() {
            mySwiper.init();
            mySwiper.slideTo(slideNum)
          }
        }
      })
    })
  })

  
  
}
var Sprite = (function() {
  function associateProgress(progress, num) {
    return Math.round((num / 100) * Math.round(progress * 100));
  }

  function Sprite(options) {
    this.ctx = options.ctx;
    this.image = options.image;
    this.frameIndex = 0;
    this.tickCount = 0;
    this.ticksPerFrame = options.ticksPerFrame || 0;
    this.numberOfFrames = options.numberOfFrames || 1;
    this.width = options.width;
    this.height = options.height;
    this.render();
  }

  var _proto = Sprite.prototype;

  /**
   * @param {number} index - number frame from 1 to numberOfFrames
   * @returns {void}
   */
  _proto.step = function step(index) {
    this.frameIndex = index;
    this.render();
  }

  /**
   * @param { number } progress: progress from 0 to 1
   * @returns {void}
   */
  _proto.progress = function progress(progress) {
    this.frameIndex = associateProgress(progress, this.numberOfFrames - 1);
    this.render();
  }

  _proto.update = function update() {
    this.tickCount += 1;
    if (this.tickCount > this.ticksPerFrame) {
      this.tickCount = 0;

      if (this.frameIndex < this.numberOfFrames - 1) {
        this.frameIndex += 1;
      } else {
        this.frameIndex = 0;
      }
    }
  }

  _proto.render = function render() {
    this.ctx.clearRect(0, 0, this.width / this.numberOfFrames, this.height);
    this.ctx.drawImage(
      this.image,
      (this.frameIndex * this.width) / this.numberOfFrames,
      0,
      this.width / this.numberOfFrames,
      this.height,
      0,
      0,
      this.width / this.numberOfFrames,
      this.height
    );
  }

  _proto.start = function start() {
    var self = this;
    var loop = function() {
      self.update();
      self.render();
      window.requestAnimationFrame(loop);
    };
    window.requestAnimationFrame(loop);
  }

  return Sprite;
}());

function getCoords(elem) {
  var box = elem.getBoundingClientRect();
  var body = document.body;
  var docEl = document.documentElement;
  var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
  var clientTop = docEl.clientTop || body.clientTop || 0;
  var clientLeft = docEl.clientLeft || body.clientLeft || 0;
  var top  = box.top +  scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;
  var width = box.width;
  var height = box.height;
  return { top: Math.round(top), left: Math.round(left), width: Math.round(width), height: Math.round(height) };
}

function boxAnimation() {
  var canvas = document.getElementById('box-canvas');
  var triggerStop = document.getElementById('trigger-stop');
  var triggerStart = document.getElementById('trigger-start');

  if(typeof gsap === 'object' && !!canvas && !!triggerStop) {
    gsap.registerPlugin(ScrollTrigger);
    var w = 598;
    var h = 800;
    var numberOfFrames = 16;

    canvas.width = w;
    canvas.height = h;
    var coinImage = new Image();
    coinImage.src = 'assets/img/images/box-sprite2.png';
    coinImage.onload = function() {
      var sprite = new Sprite({
        ctx: canvas.getContext('2d'),
        image: coinImage,
        width: w * numberOfFrames,
        height: h,
        numberOfFrames: numberOfFrames,
        ticksPerFrame: 4
      });
      var tl = gsap.timeline({
        scrollTrigger: {
          trigger: triggerStart,
          endTrigger: triggerStop,
          start: function() {return -(getCoords(triggerStart).top) + ' '+ 0},
          end: function() { return '93% ' + getCoords(triggerStart).top + '+=' + getCoords(triggerStart).height; },
          pin: canvas,
          markers: false,
          scrub: true,
          onUpdate: function(data) {
            sprite.progress(data.progress);
          }
          // onEnter: function() {
          //   console.log('onEnter')
          // },
          // onLeave: function() {
          //   console.log('onLeave')
          // },
          // onEnterBack: function() {
          //   console.log('onEnterBack')
          // },
          // onLeaveBack: function() {
          //   console.log('onLeaveBack')
          // },
        }
      });

      tl.to(canvas, {x: "-23%", duration: 500});
      scrollFirstSectionFn();
    };
  }
}

function scrollFirstSectionFn() {
  var breakpoint = window.matchMedia('(max-width: 767.98px)');
  var isScrollPrevented = false;
  var isAnimating = false;
  // left: 37, up: 38, right: 39, down: 40,
  // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
  var keys = {37: 1, 38: 1, 39: 1, 40: 1};
  // modern Chrome requires { passive: false } when adding event
  var supportsPassive = false;
  try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
      get: function () { supportsPassive = true; }
    }));
  } catch(e) {}
  var wheelOpt = supportsPassive ? { passive: false } : false;
  var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
  var $header = document.querySelector('.header');
  var $fisrstSect = document.querySelector('#first-section');
  var $secondSect = document.querySelector('#second-section');

  if($(window).scrollTop() > getCoords($fisrstSect).height && !breakpoint.matches) {
    window.addEventListener('scroll', scrolltopLister, wheelOpt);
  } else {
    breakpointHandler();
  }

  breakpoint.addListener(breakpointHandler);

  $('.js-scroll-to').on('click', function(e) {
    e.preventDefault();
    enableScroll();
    var scroll_el = $(this).attr('href');
    if ($(scroll_el).length !== 0) {
      $('html, body').stop().animate({
        scrollTop: $(scroll_el).offset().top
      }, 1000, function() {
        window.addEventListener('scroll', scrolltopLister, wheelOpt);
      });
    }
  });

  function breakpointHandler(e) {
    var target = e && e.target ? e.target : breakpoint;
    if (target.matches) {
      enableScroll();
      window.removeEventListener('scroll', scrolltopLister);
    } else if(!target.matches) {
      disableScroll();
    }
  }

  function preventDefault(e) {
    e.preventDefault();
    wheelHandler(e)
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      wheelHandler(e);
      return false;
    }
  }

  function wheelHandler(e) {
    if (e.wheelDelta > 0 || e.detail < 0) {
      // Scroll up
    } else {
      // Scroll down
      if(isScrollPrevented && !isAnimating) {
        isAnimating = true;
        $('html, body').stop().animate({
          scrollTop: getCoords($secondSect).top
        }, 1000, function() {
          enableScroll();
          isAnimating = false;
          window.addEventListener('scroll', scrolltopLister, wheelOpt);
        });
      }
    }
  }

  function scrolltopLister(e) {
    if(window.pageYOffset < getCoords($secondSect).top - getCoords($header).height / 2) {
      window.removeEventListener('scroll', scrolltopLister);
      disableScroll();
      if(isScrollPrevented && !isAnimating) {
        isAnimating = true;
        setTimeout(function() {
          $('html, body').stop().animate({ scrollTop: 0 }, 1000,
            function() {
            isAnimating = false;
          });
        }, 200);
      }
    }
  }

  // call this to Disable
  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
    isScrollPrevented = true;
  }

  // call this to Enable
  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
    isScrollPrevented = false;
  }
}

function utekaWidjet() {
  if ($(".map-block").length) {
    window.uteka = {

      /*
        * Функция для загрузки товаров в виджет
        *
        * @param {HTMLElement} widget – DOM элемент виджета
        * @param {object} options
        * @param {string} options.productId – ID выбранного товара (опционально)
        * @param {string[]} options.productIds – ID товаров для выбора через селектор товаров (опционально)
        * @param {string} options.utmTerm – UTM term (опционально)
        * @param {string} options.utmSource – UTM source (опционально)
        * @param {string} options.utmMedium – UTM medium (опционально)
        * @param {string} options.utmContent – UTM content (опционально)
        * @param {string} options.utmCampaign – UTM campaign (опционально)
        */
      loadWidget: function (widget, options) {
        if (
          !options.productId &&
          !options.productIds
        ) {
          throw new TypeError('[Виджет] Для загрузки виджета необходимо передать параметр "productId" или "productIds"')
        }
    
        if (
          options.productId &&
          options.productIds &&
          options.productIds.length > 0 &&
          options.productIds.indexOf(options.productId) === -1
        ) {
          throw new TypeError('[Виджет] Параметр "productIds" должен в себя включать идентификатор товара, переданный через параметр "productId"')
        }
    
        if (
          options.productIds &&
          options.productIds.length > 100
        ) {
          throw new TypeError('[Виджет] Максимальное количество товаров, передаваемых в "productIds" не может превышать 100 штук')
        }
    
        if (
          options.productIds &&
          options.productIds.some( function(productId) { return !/^\d+$/.test(productId)})
        ) {
          throw new TypeError('[Виджет] Параметр "productIds" содержит некорректный идентификатор товара')
        }
    
        if (
          options.productId &&
          !/^\d+$/.test(options.productId)
        ) {
          throw new TypeError('[Виджет] Параметр "productId" содержит некорректный идентификатор товара')
        }
    
        var iframe = widget.querySelector('iframe')
        var query = []
    
        if (options.productIds) {
          options.productIds.forEach(function (productId) {
            query.push('productIds=' + productId)
          })
        }
        if (options.productId) {
          query.push('productId=' + options.productId)
        }
        if (options.utmTerm) {
          query.push('utm_term=' + options.utmTerm)
        }
        if (options.utmSource) {
          query.push('utm_source=' + options.utmSource)
        }
        if (options.utmMedium) {
          query.push('utm_medium=' + options.utmMedium)
        }
        if (options.utmContent) {
          query.push('utm_content=' + options.utmContent)
        }
        if (options.utmCampaign) {
          query.push('utm_campaign=' + options.utmCampaign)
        }
    
        iframe.src = 'https://widget.uteka.ru/widgets/full/?' + query.join('&')
      }
    }

    var widget = document.querySelector('.map-block')

    window.uteka.loadWidget(widget, {
      productId: '296149'
    })
  }
  
}

function inputMask() {
  if($(".js-calendar-input").length) {
    $(".js-calendar-input").inputmask({"mask": "99.99.9999"});
  }
}

function calendarForm() {
  if($(".calendar-form .js-calendar").length) {
    $(".calendar-form .js-calendar").click(function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active").text("Календарь");
        $(this).parent().find(".datepicker-here").fadeOut();
      } else {
        $(this).addClass("active").text("Закрыть");
        $(this).parent().find(".datepicker-here").fadeIn();
      }
    });

    $(".calendar-form .datepicker-here").datepicker({
      onSelect: function onSelect(formattedDate, date, inst) {
        var val = formattedDate;
        var $block = inst.$el;
        calendarData(val, $block);
      }
    });
  }

  function calendarData(val, el) {
    el.parent().find("input").val(val)
  }
}

function initBecomeMomCalendar() {
  var calendarEl = document.getElementById('calendar-result');
  var memory = {
    events: []
  };

  if(calendarEl) {
    var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      locale: 'ru',
      headerToolbar: {
        left:'',
        center:'prev,title,next',
        right:''
      },
      firstDay: 1,
      titleFormat: 'MMMM YYYY',
      datesSet: function(event) {
        var titleArr = event.view.title.split(' ');
        var titleEl = calendar.el.querySelector('.fc-toolbar-title');
        if(titleArr.length === 2) {
          var title = '<span><span>' + titleArr[0] + '</span>, <span class="title-year">' + titleArr[1] + '</span></span>'
          titleEl.innerHTML = '';
          titleEl.innerHTML = title;
        }

        setTimeout(function() {
          var days = Array.prototype.slice.call(calendar.el.querySelectorAll('.fc-daygrid-day.fc-day'));

          days.forEach(function(day) {
            var event1 = day.querySelector('.fc-event.fertile-day');
            var event2 = day.querySelector('.fc-event.ovulation-day');

            if(event1 || event2) {
              day.classList.add('fc-has-event');
            }
          });
        }, 0);
      }
    });
  }

  return {
    calendar: calendar,
    memory: memory
  };
}

function calculatorOvulation() {
  var cld = initBecomeMomCalendar();
  var calendar = cld.calendar;
  var memory = cld.memory;
  var step = 6;
  var maxMonth;

  $('.become-mom-calcualator').on('click', '.fc-next-button', function(e) {
    var self = this;
    var $parent = $(self).closest('.become-mom-calcualator');
    var form = $parent.find('.js-become-mom-form')[0];
    var cDuration = +form.querySelector('[name="c-duration"]').value;
    var mDuration = +form.querySelector('[name="m-duration"]').value;
    var cFirstDay = moment(form.querySelector('[name="c-first-day"]').value, 'DD.MM.YYYY');

    if(cDuration && mDuration && cFirstDay.isValid()) {
      var montsCount = Math.ceil(moment(calendar.getDate()).diff(cFirstDay, 'months', true));

      if(montsCount % 4 === 0 && montsCount > maxMonth) {
        maxMonth = montsCount;
        getEvents(cDuration, mDuration, cFirstDay.toDate(), step);
        showResultCalendar($parent, calcCalendarEvents(cDuration, step));
      }
    }
  });

  $('.js-become-mom-form').on("submit", function(e) {
    e.preventDefault();
    var self = this;
    var $parent = $(self).closest('.become-mom-calcualator');
    var cDuration = +self.querySelector('[name="c-duration"]').value;
    var mDuration = +self.querySelector('[name="m-duration"]').value;
    var cFirstDay = moment(self.querySelector('[name="c-first-day"]').value, 'DD.MM.YYYY');

    if(memory.events.length) {
      memory.events.length = 0;

      calendar.getEvents().forEach(function(event) {
        event.remove();
      });
    }

    if(cDuration && mDuration && cFirstDay.isValid()) {
      calendar.gotoDate(cFirstDay.toDate());
      var montsCount = Math.ceil(moment(calendar.getDate()).diff(cFirstDay, 'months', true));

      if(montsCount >= 0) {
        maxMonth = montsCount;
        getEvents(cDuration, mDuration, cFirstDay.toDate(), step);
        showResultCalendar($parent, calcCalendarEvents(cDuration, step));
      }
    }
  });

  function calcCalendarEvents(cDuration, step) {
    var monthRender = step || 1;

    return  function() {
      memory.events.forEach(function(event, i) {
        if(typeof event === 'object' && event !== null && i >= memory.events.length - cDuration * monthRender) {
          calendar.addEvent(event);
        }
      });

      calendar.render();
    }
  }

  function getEvents(cycDuration, minsDuration, cycFirstDay, step) {
    var d = Array(cycDuration);
    var monthRender = step || 1;
    var k = memory.events.length;
    k = k ? k + 1 : k;

    for(var j = 0; j < cycDuration; j++) {
      if(j >= cycDuration - 18 && j < cycDuration - 11 - 4 || j > cycDuration - 11 - 4 && j < cycDuration - 11) {
        d[j] = 'fertile-day';
      }

      if(j < minsDuration) {
        d[j] = 'menstruation-day';
      }

      if(j === cycDuration - 11 - 4) {
        d[j] = 'ovulation-day';
      }
    }

    for(var i = 0; i < monthRender; i++) {
      for(var j = 0; j < cycDuration; j++) {
        if(d[j]) {
          var event = getEvent(k, d[j], cycFirstDay);
          memory.events.push(event);
        } else if(j === cycDuration - 1) {
          memory.events.push('last');
        } else {
          memory.events.push(null);
        }
        k++;
      }
    }
  }

  function getEvent(k, clsName, cycFirstDay) {
    return {
      id: k,
      start: moment(cycFirstDay, "DD.MM.YYYY").add(k, 'days').format('YYYY-MM-DD'),
      end: moment(cycFirstDay, "DD.MM.YYYY").add(k, 'days').format('YYYY-MM-DD'),
      display: clsName ? 'background' : 'none',
      classNames: clsName
    };
  }

  function showResultCalendar($parent, cb) {
    if(!$parent.hasClass('result')) {
      if(calendar) {
        calendar.render();
        $(calendar.el).css('opacity', 0);
      }

      typeof cb === 'function' && cb();

      $parent.addClass('result').find('.calcualator__result').fadeIn(function() {
        if(calendar) {
          calendar.render();
          reflow(calendar.el);
          $(calendar.el).css('opacity', 1);
        }
      });
    } else {
      typeof cb === 'function' && cb();
    }
  }
}

function calcUlatorWeightHeight() {
  var $calculators = $('.calcualator-wheight-form');

  $calculators.each(function (_, c) {
    var $calculator = $(c);
    var $genderControls = $calculator.find('input[name="gender"]');

    $genderControls.on('change', function(e) {
      var $self = $(this);
      var $currentStep = $self.closest('.calcualator__step');
      var $nextStep = $calculator.find("[data-step=" + $self.val() + "]");

      $nextStep.find('input.calcualator-input-height').attr('name', 'height');
      $nextStep.find('input.calcualator-input-weight').attr('name', 'weight');
      $nextStep.find('input.calcualator-input-birddate').attr('name', 'bird_date');

      $currentStep.fadeOut(function() {
        $nextStep.fadeIn();
      });
    });

    $calculator.on('submit', function(e) {
      var height = $calculator.find('input[name="height"]');
      var weight = $calculator.find('input[name="weight"]');
      var birdDate = $calculator.find('input[name="bird_date"]');

      if(!(height.val() && weight.val() && birdDate.val())) {
        e.preventDefault();
      }
    });
  });
}

function calculatorPregnacy() {
  var $calculators = $('.calcualator-pregnancy');

  $calculators.each(function (_, c) {
    var $calculator = $(c);
    var $datesControls = $calculator.find('input[name="date_type"]');

    $datesControls.on('change', function(e) {
      var $self = $(this);
      var $currentStep = $self.closest('.calcualator__step');
      var $nextStep = $calculator.find("[data-step=" + $self.val() + "]");

      $nextStep.find('input.calendar-input-date').attr('name', 'date');

      $currentStep.fadeOut(function() {
        $nextStep.fadeIn();
      });
    });

    $calculator.on('submit', function(e) {
      var date = $calculator.find('input[name="date"]');

      if(!date.val()) {
        e.preventDefault();
      }
    });
  });
}

function calculatorCommonFn(options) {
  var prevStep = "";
  var nextStep = "";

  $("[data-go-to]").click(function(e){
    e.preventDefault();
    var $self = $(this);
    prevStep = nextStep;
    nextStep = $self.attr("data-go-to");

    if (nextStep != "result" && nextStep != "result-1" && nextStep != "result-2") {
      var $currentStep = $self.closest("[data-step]");
      var $nextStep = $("[data-step="+nextStep+"]");

      // clear side effects
      if($currentStep.attr('data-step') === 'boy' || $currentStep.attr('data-step') === 'girl') {
        $currentStep.find('input.calcualator-input-height, input.calcualator-input-weight, input.calcualator-input-birddate').removeAttr('name');
      }

      if($currentStep.attr('data-step') === 'date_last_cile' || $currentStep.attr('data-step') === 'date_conception') {
        $currentStep.find('input.calendar-input-date').removeAttr('name');
      }

      if($nextStep.attr('data-step') === '1') {
        $nextStep.find('input[name="gender"]').prop('checked', false);
      }
      // clear side effects

      $currentStep.fadeOut(function() {
        $nextStep.fadeIn();
      });
    } else {
      if ($(".calcualator").hasClass("result") != true ) {
        $(".calcualator").addClass("result");
        $("[data-step="+nextStep+"]").fadeIn(function() {
          $(".js-next").removeClass("btn-fill-blue").addClass("btn-fill-pink-light-2").text("Заново");
        });
      } else {
        $(".calcualator").removeClass("result");
        $(this).closest("[data-step]").fadeOut(function() {
          $("[data-step=1]").fadeIn();
          $(".js-next").removeClass("btn-fill-pink-light-2").addClass("btn-fill-blue").text("Рассчитать");
        });
      }
    }
  });

  $(".js-prev").click(function(e){
    $(".calcualator").removeClass("result");
    $(".calcualator__result").fadeOut(function() {
      $(".js-next").removeClass("btn-fill-pink-light-2").addClass("btn-fill-blue").text("Рассчитать");
    });
  });
}
