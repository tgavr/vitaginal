<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildValuesTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('child_values', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string('name');
            $table->integer('stars');
            $table->text('text');
            $table->text('link')->nullable();
            $table->text('site')->nullable();
            $table->text('text3')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('child_values', function (Blueprint $table) {
            
            $table->dropColumn('imt1');
            $table->dropColumn('imt2');

        });
    }
}
