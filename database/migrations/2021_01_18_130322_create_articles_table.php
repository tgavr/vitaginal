<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->string('banner')->nullable();
            $table->string('small_banner')->nullable();
            $table->text('tiser')->nullable();
            $table->text('description')->nullable();
            $table->text('literature')->nullable();
            $table->text('text')->nullable();

            $table->string('link')->nullable();
            $table->string('link_text')->nullable();

            $table->boolean('on_main')->default(0);
            $table->boolean('on_stage1')->default(0);
            $table->boolean('on_stage2')->default(0);
            $table->boolean('on_stage3')->default(0);

            $table->timestamps();
        });

         Schema::create('article_articles', function (Blueprint $table) {
            $table->bigInteger('artcle_main_id');
            $table->bigInteger('artcle_id');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
        Schema::dropIfExists('article_articles');
    }
}
