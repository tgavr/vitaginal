<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_values', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('gender')->nullable();
            $table->string('mounths')->nullable();
            $table->float('height_min')->nullable();
            $table->float('height_max')->nullable();
            $table->float('weight_min')->nullable();
            $table->float('weight_max')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_values');
    }
}
