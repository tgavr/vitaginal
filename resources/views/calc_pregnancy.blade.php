@extends('layouts.site')

@section('seo')
<title>Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша</title>
<meta name="keywords" content="">
<meta name="description" content="Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша">
@endsection

@section('css')
<link rel="stylesheet" href="/assets/libs/air-datepicker/css/datepicker.min.css">
<link rel="stylesheet" href="/assets/libs/fullcalendar-5.5.1/main.min.css">
@endsection

@section('content')

<main class="main offset-header">
  <article class="article">
    <div class="container">
      <div class="article__inner">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti">Хочу стать мамой</a>
            </li>
            <li>
              <a href="/calculator_pregnancy">Калькулятор беременности</a>
            </li>
          </ul>
        </div>

        <h1>Калькулятор беременности</h1>

        <div class="calcualator calcualator--pregnancy"> <!--toggle clsass .result -->
          <form class="calcualator-pregnancy">

            <div class="calcualator__step calcualator__step--1" data-step="1" @if($date) style="display: none;" @endif>
              <div class="calcualator__step-row">
                <label class="calcualator__field">
                  <img src="/assets/img/images/calcualator/calendar.svg" alt="">
                  <input type="radio" name="date_type" value="date_last_cile" @if($date_type == "date_last_cile") checked @endif>
                  <b> Дата начала последнего менструального цикла</b>
                </label>
                <label class="calcualator__field">
                  <img src="/assets/img/images/calcualator/sex.svg" alt="">
                  <input type="radio" name="date_type" value="date_conception" @if($date_type == "date_conception") checked @endif>
                  <b>Предполагаемая <br>дата зачатия</b>
                </label>
              </div>
              <b>Выберите одно из двух значений, которое Вам известно</b>
            </div>


            <div class="calcualator__step calcualator__step--2" data-step="date_conception" @if($date && $date_type == "date_conception") @else style="display: none;" @endif>
              <div class="calcualator__field">
                <b>Предполагаемая дата зачатия</b>
                <div class="calendar-form">
                  <input class="js-calendar-input calendar-input-date" type="text" placeholder="__.__.____" value="{{$date}}">
                  <button @if($date_type == "date_conception") name="date" @endif type="button" class="btn btn-fill-pink-light-2 js-calendar">Календарь</button>
                  <div class="datepicker-here"></div>
                </div>
              </div>

              @if ($state)<b style="margin-bottom: 2rem">{{ $state }}</b>@endif

              <button type="submit" class="btn btn-fill-blue calcualator__step-action-btn">
                Рассчитать
              </button>

              <!-- <a href="#" class="btn btn-fill-pink-light-2 calcualator__step-action-btn js-next" data-go-to="1">
                Заново
              </a> -->
            </div>

            <div class="calcualator__step calcualator__step--2" data-step="date_last_cile" @if($date && $date_type == "date_last_cile") @else style="display: none;" @endif>
              <div class="calcualator__field">
                <b>Дата начала последнего менструального цикла</b>
                <div class="calendar-form">
                  <input class="js-calendar-input calendar-input-date" @if($date_type == "date_last_cile") name="date" @endif type="text" placeholder="__.__.____" value="{{$date}}">
                  <button type="button" class="btn btn-fill-pink-light-2 js-calendar">Календарь</button>
                  <div class="datepicker-here"></div>
                </div>
              </div>

              @if ($state)<b style="margin-bottom: 2rem">{{ $state }}</b>@endif

              <button type="submit" class="btn btn-fill-blue calcualator__step-action-btn">
                Рассчитать
              </button>

              <!-- <a href="#" class="btn btn-fill-pink-light-2 calcualator__step-action-btn js-next" data-go-to="1">
                Заново
              </a> -->
            </div>


            @if ($week)
            <div class="calcualator__result" data-step="result" >
              <div class="calcualator__result-content">
                @if($pdr)Приблизительная<br>дата рождения:<br><b>{{ $pdr }}</b>@endif
                <hr>
                Акушерска неделя:<br><b>{{ $week->num }} неделя</b>
              </div>
              <div class="calcualator__baby-size grapefruit">
                <img src="/{{ $week->fruit_img }}" alt="">
                <div style="width: 15rem;text-align: center;">{{ $week->fruit }}</div>
              </div>
            </div>

            @endif

          </form>
        </div>

        @if ($week)
        <script type="text/javascript">
          var VARresult = true;
        </script>
        <div data-step="result">
          <div class="flip-article-head">
            <a href="/calculator_pregnancy?week={{ $week->num - 1 }}&date_type={{ $date_type }}&date={{ $date }}" class="flip-article-head__btn prev">{{ $week->num - 1 }} неделя</a>
            <div class="flip-article-head__title">
              {{ $week->num }} неделя  <br>беременности
            </div>
            <a href="/calculator_pregnancy?week={{ $week->num + 1 }}&date_type={{ $date_type }}&date={{ $date }}" class="flip-article-head__btn next">{{ $week->num + 1 }} неделя</a>
          </div>

          <img src="/assets/img/images/img-article.jpg" alt="">

          <div class="article__content">
            {!! $week->text1 !!}

            <p style="
              border: solid #fedae6 4px;
              padding: 9px;
          "><a href="/about" style="
              color: #613769;
              text-decoration: underline;
          ">Витажиналь®</a> – базовый комплекс микронутриентов для здоровья мамы и малыша.</p>

            @if ($week->text2)
            <blockquote>
              {!! $week->text2 !!}
            </blockquote>
            @endif
            <!-- <div class="uses-acco hide-tablet js-acco">
              <div class="uses-acco-item js-acco-item">
                <div class="uses-acco-item__head js-acco-head">
                  <span class="uses-acco-item__ico">
                    <img src="/assets/img/icons/acco-ico-1.svg" alt="">
                  </span>
                  <span><u>Список используемой литературы</u></span>
                  <span class="btn btn-fill-pink-light">Смотреть</span>
                </div>
                <div class="uses-acco-item__body js-acco-body">
                  <div class="uses-acco-item__content">
                    <ul>
                      <li>
                         <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                      </li>
                      <li>
                         <strong>Василевский И.В.</strong> Современные возможности организации питания детей первого года жизни. Педиатрия. Восточная Европа. 2014. № 1 (05). С. 157-165.
                      </li>
                      <li>
                         <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                      </li>
                      <li>
                         <strong>Василевский И.В.</strong> Современные возможности организации питания детей первого года жизни. Педиатрия. Восточная Европа. 2014. № 1 (05). С. 157-165.
                      </li>
                      <li>
                         <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
        @endif
      </div>
    </div>
  </article>

  <div class="useful-articles pink-wrapper">
    <div class="container">

      <div class="section-small-title">
        Полезные статьи
      </div>

      <div class="useful-articles-slider hide-phone-lg">
        <div class="swiper-container">
          <div class="swiper-wrapper">

            @foreach ($articles as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($articles->slice(0, 3) as $a)

        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>

    </div>
  </div>
</main>
@endsection


@section('js')
<script src="/assets/libs/jquery.inputmask.min.js"></script>
<script src="/assets/libs/air-datepicker/js/datepicker.min.js"></script>
@endsection