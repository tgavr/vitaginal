@extends('layouts.site')

@section('seo')
<title>{{ $page->title }}</title>
<meta name="keywords" content="">
<meta name="description" content="{{ $page->description }}">
@endsection

@section('content')

<main class="main">

  <section class="elements-life elements-life--about-product">
    <div class="container">
      <div class="elements-life__cover">
        <div class="elements-life__elements elements-life__elements--about-product">
          <img class="step-hero__emblem" src="assets/img/icons/screen-label.svg" alt="">
          <img class="elements-life__product" src="/assets/img/images/elements-life/product-4.png" alt="">
          <a href="/mark.pdf" target="_blank"><img class="elements-life__reward" src="/assets/img/images/elements-life/reward.svg" alt=""></a>
        </div>
        
        <div class="elements-life__content">
          <h2 class="main-title"><strong>Витажиналь<sup>&reg;</sup> – </strong><br>5 элементов для новой жизни <span class="elements-life__subtitle">и ничего лишнего</span></h2>

          <div class="elements-life__properties-row">
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico elements-properties__ico--kubs">
                  <img src="/assets/img/images/elements-life/kubs.svg" alt="">
                </div>
                <div>Физиологические дозы компонентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/micronutrients.svg" alt="">
                </div>
                <div>Рациональное сочетание микронутриентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/five.svg" alt="">
                </div>
                <div>Пять базовых микронутриентов для здоровья мамы и малыша</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/quality.svg" alt="">
                </div>
                <div>Французское <br>качество</div>
              </div> 
            </div>
          </div>
        </div>
        
      </div>
      <div class="useful-articles-slider useful-articles-slider--become-mom-elements space-between-20-10" >
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-b9">
                  <span>B<sub>9</sub></span>   
                </div>
                <div class="elem-card__title">
                  Фолиевая кислота
                </div>
                <div class="elem-card__doze">
                  400 мкг (0,4 мг)
                </div>
                <div class="elem-card__desc">
                  Снижает риск развития врожденных пороков и дефектов нервной системы ребенка. Важно начать ее прием еще до зачатия.
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-i">
                  <span>I</span>
                </div>
                <div class="elem-card__title">
                  Йод
                </div>
                <div class="elem-card__doze">
                  150 мкг
                </div>
                <div class="elem-card__desc">
                  Важен для нормальной работы щитовидной железы, ответственной за процессы зачатия, беременности и родов. 
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-omega3">
                  <span>Ω<sub>3</sub></span> 
                </div>
                <div class="elem-card__title">
                  Омега-3 (ДГК)
                </div>
                <div class="elem-card__doze">
                  200 мг
                </div>
                <div class="elem-card__desc">
                  Необходима для подготовки к беременности, увеличивает вероятность зачатия и способствует формированию сосудов плаценты, если беременность состоялась.  
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-d3">
                  <span>D<sub>3</sub></span>
                </div>
                <div class="elem-card__title">
                  Витамин D
                </div>
                <div class="elem-card__doze">
                  200 МЕ (5 мкг)
                </div>
                <div class="elem-card__desc">
                  Участвует в правильной работе репродуктивной системы. Регулирует обмен кальция и фосфора в организме.
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-e">
                  <span>E</span>
                </div>
                <div class="elem-card__title">
                  Витамин Е
                </div>
                <div class="elem-card__doze">
                  12 мг
                </div>
                <div class="elem-card__desc">
                  Антиоксидант, участвует в синтезе гормонов. Способствует созреванию яйцеклеток, без которых зачатие невозможно. Защищает Омега-3 от окисления.
                </div>
              </div>
            </div>



          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>
    </div>
  </section>
  <div class="about-section-bg">
    <div class="about-video">
      <div class="container">
        <script async type="text/javascript" src="//asset.fwcdn3.com/js/embed-feed.js"></script>
    <fw-embed-feed 
      channel="besins_channel"
playlist="o3XrKg"
      mode="row"
      open_in="default"
      max_videos="0"
      placement="middle"
      player_placement="bottom-right"></fw-embed-feed>
        <div class="video-wrap">
          <div class="video-bg">
            <img class="hide-tablet" src="/assets/img/images/video-bg.jpg" alt="">
            <img class="show-tablet" src="/assets/img/images/video-bg-mob.jpg" alt="">
            <a class="play" href="#" data-video-src="https://www.youtube.com/embed/l-L_xTXP_EM"><img src="/assets/img/icons/play.svg" alt=""></a>
          </div>
          <iframe class="video" width="100%" height="100%" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    </div>

    <section class="importance-block">
      <div class="container">
        <div class="importance-block__cover">
          <div class="importance-block__col">
            <h2 class="main-title">О важности микронутриентов во время беременности</h2>
          </div>
          <div class="importance-block__col">
            <p>Во время беременности организм женщины нуждается в повышенном потреблении ряда питательных веществ и микронутриентов.</p>
            <p>Дефицит некоторых из них - фактор риска развития врожденных пороков у плода, преждевременных родов, рождения детей с малой массой тела и т.д.
            </p>
            <p>Однако не все витамины и минералы во время беременности необходимы, а избыток - не всегда польза для здоровья, тем более во время беременности. 
            </p>
          </div>
        </div>
        <div class="importance-block__img">
          <img src="/assets/img/images/img1.jpg" alt="">
        </div>
      </div>
    </section>

    <section class="they-good">
      <div class="container">
        <h2 class="main-title">Всегда ли хороши мультивитаминные комплексы для беременных?</h2>
        <div class="they-good__row">
          <div class="they-good__col">
            <div class="they-good-card">
              Чем сложнее состав витаминно-минеральных комплексов, тем затруднительней всасывание каждого витамина по- отдельности.
            </div>
          </div>
          <div class="they-good__col">
            <div class="they-good-card">
              Большое количество компонентов может увеличить фармакологическую нагрузку на организм.
            </div>
          </div>
          <div class="they-good__col">
            <div class="they-good-card">
              Избыток поступающих витаминов и минералов может привести к изменению витаминного баланса в организме.
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <section class="why-block" style="background: transparent;">
    <div class="container">
      <div class="main-title">Почему <br>Витажиналь<sup>&reg;</sup></div>
      <div class="why-block__row">
        <div class="why-block__col">
          <div class="why-block-card">
            <div class="why-block-card__img">
              <img src="/assets/img/images/complex.svg" alt="">
            </div>
            <strong>Витажиналь<sup>&reg;</sup> — комплекс из пяти важнейших микронутриентов</strong> необходимых для подготовки к зачатию, беременности и лактации.
          </div>
        </div>
        <div class="why-block__col">
          <div class="why-block-card">
            <div class="why-block-card__img">
              <img src="/assets/img/images/vitamins.svg" alt="">
            </div>
            <strong>Витажиналь<sup>&reg;</sup> — рациональный подход к витаминно-минеральной поддержке</strong> 
            оптимальное сочетание базовых микронутриентов для здоровья мамы и малыша
          </div>
        </div>
        <div class="why-block__col">
         <div class="why-block-card">
            <div class="why-block-card__img">
              <img src="/assets/img/images/vitamins-bottle.svg" alt="">
            </div>
            <strong>Витажиналь<sup>&reg;</sup> можно сочетать с отдельными витаминами или минералами</strong> в индивидуально подобранной дозе в случае выявления их дефицита
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="product-substance" style="background: #FFF6F8;">
    <div class="container">
      <div class="product-substance__cover">
        <img src="/assets/img/images/img-22.png" alt="" class="hide-tablet product-substance__img">
        <img src="/assets/img/images/img-22-mob.png" alt="" class="show-tablet product-substance__img">    
        <h2 class="main-title">Витажиналь<sup>&reg;</sup> - содержит группу Омега-3, которые важны уже с ранних сроков беременности</h2>
        <p>Витажиналь<sup>&reg;</sup> - базовый комплекс микронутриентов, содержащий незаменимую Омега-3 ПНЖК - докозагексаеновую кислоту (ДГК), которая играет важную роль в процессе подготовки к зачатию, беременности и лактации.</p>
        <p>Омега-3 полиненасыщенные жирные кислоты в составе комплекса Витажиналь<sup>®</sup> получают из высокоочищенного сырья в самых чистых точках мирового океана планеты. Специальная технология очистки компании POLARIS® (Франция) позволяет получить необходимую безопасность</p>
      </div>
    </div>
  </section>

  <section class="order-block white-wrapper">
    <div class="container">
      <div class="section-small-title">
        Где купить Витажиналь<sup>&reg;</sup>
      </div>

      <div class="pharmacies" id="pharmacies">
        <div class="pharmacies__row">
          @foreach ($pharmacies as $pharmacy)
          <div class="pharmacies__col">
            <a @if ($pharmacy->link)href="{{ $pharmacy->link }}"@endif class="pharmacies-card">
              <div class="pharmacies-card__inner">
                <img src="/{{ $pharmacy->logo }}" alt="">
              </div>
            </a>
          </div>
          @endforeach
        </div>
      </div>

      <div class="order-block__actions">
        <a href="#" class="btn btn-fill-pink js-show-all" data-target="#pharmacies">
          <span class="btn__inner">
            <span class="btn__text">
              <span class="btn__toggle">
                <span class="btn__toggle-unactive">Показать все аптеки</span>
                <span class="btn__toggle-active">Скрыть</span>
              </span>
            </span>
          </span>
        </a>
      </div>

      <div class="map-block">
        <!-- <img src="/assets/img/images/fake-map.jpg" alt=""> -->
        <div class="uteka-widget">
          <!-- Шапка виджета -->
          <div class="uteka-widget-header">
            <div class="uteka-widget__container">
              <div class="uteka-widget-header__inner">
                <!-- Логотип -->
                <a
                  class="uteka-widget-header__logo"
                  href="https://uteka.ru/"
                  target="_blank"
                >
                  <img
                    src="https://widget.uteka.ru/static/img/widgets/logo-light.svg"
                    alt="Заказать в Ютеке"
                    title="поиск в аптеках"
                  />
                </a>
        
                <!-- Заголовок -->
                <div class="uteka-widget-header__title"></div>
        

              </div>
            </div>
          </div>
        
          <iframe allow="geolocation"></iframe>
        </div>
      </div>

      
      <div class="uses-acco js-acco">
        <div class="uses-acco-item js-acco-item">
          <div class="uses-acco-item__head js-acco-head">
            <span class="uses-acco-item__ico">
              <img src="/assets/img/icons/acco-ico-1.svg" alt="">
            </span>
            <span><u>Список используемой литературы</u></span>
            <span class="btn btn-fill-pink-light">Смотреть </span>
          </div>
          <div class="uses-acco-item__body js-acco-body">
            <div class="uses-acco-item__content">
              {!! $page->literature !!}
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>


</main>
@endsection