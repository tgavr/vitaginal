@extends('layouts.site')

@section('seo')
<title>{!! $article->title !!}</title>
<meta name="keywords" content="">
<meta name="description" content="{{ $article->description }}">
@endsection

@section('content')

<main class="main offset-header">
  <article class="article">
    <div class="container">
      <div class="article__inner">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a href="/articles">Статьи</a>
            </li>
            <li>
              <a>{!! $article->title !!}</a>
            </li>
          </ul>
        </div>

        <h1>{!! $article->title !!}</h1>
        <img src="/{{ $article->banner }}" alt="">

        <div class="article__content">
          {!! $article->text !!}

          @if ($article->link)
          <a href="/{{ $article->link }}" target="" download class="file-download">
            <span class="file-download__ico">
              <img src="/assets/img/icons/pdf-ico.svg" alt="">
            </span>
            <span class="file-download__label">
              <strong>{{ $article->link_text }}</strong> [Скачать в PDF]
            </span>
          </a>
          @endif

          @if ($article->literature)
          <div class="uses-acco hide-tablet js-acco">
            <div class="uses-acco-item js-acco-item">
              <div class="uses-acco-item__head js-acco-head">
                <span class="uses-acco-item__ico">
                  <img src="/assets/img/icons/acco-ico-1.svg" alt="">
                </span>
                <span><u>Список используемой литературы</u></span>
                <span class="btn btn-fill-pink-light">Смотреть</span>
              </div>
              <div class="uses-acco-item__body js-acco-body">
                <div class="uses-acco-item__content">
                  {!! $article->literature !!}
                </div>
              </div>
            </div>
          </div>
          @endif
        </div>

      </div>
    </div>
  </article>

  <div class="useful-articles pink-wrapper">
    <div class="container">

      <div class="section-small-title">
        Полезные статьи
      </div>

      <div class="useful-articles-slider hide-phone-lg">
        <div class="swiper-container">
          <div class="swiper-wrapper">

            @foreach ($article->articles()->get() as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($article->articles()->get()->slice(0, 3) as $a)
        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>

      @if ($article->literature)
      <div class="uses-acco show-tablet js-acco">
        <div class="uses-acco-item js-acco-item">
          <div class="uses-acco-item__head js-acco-head">
            <span class="uses-acco-item__ico">
              <img src="/assets/img/icons/acco-ico-1.svg" alt="">
            </span>
            <span><u>Список используемой литературы</u></span>
            <span class="btn btn-fill-pink-light">Смотреть </span>
          </div>
          <div class="uses-acco-item__body js-acco-body">
            <div class="uses-acco-item__content">
              {!! $article->literature !!}
            </div>
          </div>
        </div>
      </div>
      @endif

    </div>
  </div>
</main>
@endsection