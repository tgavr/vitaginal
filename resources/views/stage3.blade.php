@extends('layouts.site')

@section('seo')
<title>{{ $page->title }}</title>
<meta name="keywords" content="">
<meta name="description" content="{{ $page->description }}">
@endsection

@section('content')
<!-- main -->
<main class="main">
  <section class="step-hero step-hero--was-born">
    <div class="container">
      <div class="step-hero__content offset-header">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="index.html">Главная</a>
            </li>
            <li>
              <a href="was-born.html">Мама, я родился</a>
            </li>
          </ul>
        </div>
        <h1 class="main-title">Мама, я родился!</h1>
        <div class="step-hero__desc">
          <p>Вот и настал счастливый момент. <br>Вы – мама!  Витажиналь<sup>&reg;</sup> расскажет, как сделать этот период комфортным для себя и малыша.</p>
        </div>
        <a href="#to-next" class="btn btn-fill-pink step-hero__down-link scroll-to">Подробнее</a>
      </div>
      <img class="step-hero__emblem" src="assets/img/icons/screen-label-1.svg" alt="">
    </div>
  </section>
  <section class="elements-life elements-life--was-born" id="to-next">
    <div class="container">
      <div class="elements-life__cover">
        <div class="elements-life__elements elements-life__elements--was-born">
          <div class="elements-life__toy-cube" data-parallax='{"y": -150}'>
            <img class="js-parallax-1" src="/assets/img/images/elements-life/toy-cube.svg" alt="">
          </div>
          <div class="elements-life__bottle-2" data-parallax='{"y": -150}'>
            <img class="js-parallax-2"  src="/assets/img/images/elements-life/bottle-2.svg" alt="">
          </div>
          <div class="elements-life__duck-toy" data-parallax='{"y": -150}'>
            <img class="js-parallax-2"  src="/assets/img/images/elements-life/duck-toy.svg" alt="">
          </div>
          <img class="elements-life__product" src="/assets/img/images/elements-life/product-3.png" alt="">
          <img class="elements-life__reward" src="/assets/img/images/elements-life/reward.svg" alt="">
        </div>
        <div class="elements-life__content">
          <h2 class="main-title">Витажиналь<sup>&reg;</sup> – <br>5 элементов для новой жизни <span class="elements-life__subtitle">и ничего лишнего</span></h2>
        
          <div class="elements-life__properties-row">
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico elements-properties__ico--kubs">
                  <img src="/assets/img/images/elements-life/kubs.svg" alt="">
                </div>
                <div>Физиологические дозы компонентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/micronutrients.svg" alt="">
                </div>
                <div>Рациональное сочетание микронутриентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/five.svg" alt="">
                </div>
                <div>Пять базовых микронутриентов для здоровья мамы и малыша</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/quality.svg" alt="">
                </div>
                <div>Французское <br>качество</div>
              </div> 
            </div>
          </div>
        </div>
        
      </div>
      <div class="useful-articles-slider useful-articles-slider--become-mom-elements space-between-20-10" >
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-b9">
                  <span>B<sub>9</sub></span>   
                </div>
                <div class="elem-card__title">
                  Фолиевая кислота
                </div>
                <div class="elem-card__desc">
                  Очень важна в период лактации. Способствует насыщению фолатами молока и оптимальному процессу развития организма новорожденного.

                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-i">
                  <span>I</span>
                </div>
                <div class="elem-card__title">
                  Йод
                </div>
                <div class="elem-card__desc">
                  Помогает поддержать и продлить грудное вскармливание. Дефицит йода может отражаться на умственных способностях ребенка и приводить к дефициту роста и нарушениям социальной адаптации. 
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-omega3">
                  <span>Ω<sub>3</sub></span> 
                </div>
                <div class="elem-card__title">
                  Омега-3
                </div>
                <div class="elem-card__desc">
                  Не синтезируется в организме самостоятельно. Важна для созревания головного мозга и сетчатки глаза. Способствует улучшению умственных способностей ребенка. Оказывает иммуномодулирующее, противовоспалительное, антиоксидантное действие для мамы и малыша. Способствует снижению риска послеродовой депрессии мамы.
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-d3">
                  <span>D<sub>3</sub></span>
                </div>
                <div class="elem-card__title">
                  Витамин D
                </div>
                <div class="elem-card__desc">
                  Способствует нормальной лактации. Отвечает за укрепление костей, деятельность нервной системы, стимулирует иммунитет мамы и малыша
                </div>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="elem-card">
                <div class="elem-card__ico elem-card__ico--vit-e">
                  <span>E</span>
                </div>
                <div class="elem-card__title">
                  Витамин Е
                </div>
                <div class="elem-card__desc">
                  Положительно влияет на функции скелетной мускулатуры, сердца, сосудов и половых желез. Участвует в процессе роста клеток, производстве белков и гемоглобина.
                </div>
              </div>
            </div>



          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>
    </div>
  </section>


  <div class="useful-articles pink-wrapper useful-articles--success-criteria">
    <!-- <script async type="text/javascript" src="//asset.fwcdn3.com/js/embed-feed.js"></script>
    <fw-embed-feed 
      channel="besins_channel"
playlist="o3XrKg"
      mode="row"
      open_in="default"
      max_videos="0"
      placement="middle"
      player_placement="bottom-right"></fw-embed-feed> -->
    <div class="container">

      <div class="section-small-title">
        Пять составляющих <br>здоровья малыша
      </div>

      <div class="useful-articles-slider hide-phone-lg space-between-20-10">
        <div class="swiper-container">
          <div class="swiper-wrapper">

            @foreach ($articles as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($articles->slice(0, 3) as $a)

        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>
      
      <div class="uses-acco js-acco uses-acco">
        <div class="uses-acco-item js-acco-item">
          <div class="uses-acco-item__head js-acco-head">
            <span class="uses-acco-item__ico">
              <img src="assets/img/icons/acco-ico-1.svg" alt="">
            </span>
            <span><u>Список используемой литературы</u></span>
            <span class="btn btn-fill-pink-light">Смотреть </span>
          </div>
          <div class="uses-acco-item__body js-acco-body">
            <div class="uses-acco-item__content">
              {!! $page->literature !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</main>
<!-- /main -->
@endsection