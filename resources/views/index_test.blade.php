@extends('layouts.site')

@section('seo')
<title>{{ $page->title }}</title>
<meta name="keywords" content="">
<meta name="description" content="{{ $page->description }}">
@endsection

@section('content')

<main class="main home-main offset-header">
      <section class="hero" id="first-section">
        <div class="container">
          <div class="hero__content">

            <div class="hero-content">
              <div class="section-title">
                Витажиналь<sup>®</sup> – <br> базовый комплекс <br> микронутриентов
                <small>для здоровья мамы и малыша</small>
              </div>

              <div class="home-scroll-btns">
                <ul>
                  <li>
                    <a href="#home-screen-1" class="home-scroll-btn scroll-to active">
                      <img src="/assets/img/images/spot-1.svg" alt="" class="home-scroll-btn__ico home-scroll-btn__ico--1">
                      <span>планирование <br> беременности</span>
                    </a>
                  </li>
                  <li>
                    <a href="#home-screen-2" class="home-scroll-btn scroll-to">
                      <img src="/assets/img/images/spot-2.svg" alt="" class="home-scroll-btn__ico home-scroll-btn__ico--2">
                      <span> беременность<br><br></span>
                    </a>
                  </li>
                  <li>
                    <a href="#home-screen-3" class="home-scroll-btn scroll-to">
                      <img src="/assets/img/images/spot-3.svg" alt="" class="home-scroll-btn__ico home-scroll-btn__ico--3">
                      <span>кормление <br> малыша</span>
                    </a>
                  </li>
                </ul>
              </div>

              <div class="hero__actions">
                <a href="#second-section" class="btn btn-fill-pink hide-tablet js-scroll-to">
                  <span class="btn__inner">
                    <span class="btn__text">Подробнее</span>
                  </span>
                </a>
                <a href="/about" class="btn btn-fill-blue">
                  <span class="btn__inner">
                    <span class="btn__text">О комплексе</span>
                  </span>
                </a>
              </div>
            </div>

            <div class="hero-slider">
              <div class="swiper-container">
                <div class="swiper-wrapper">

                  <div class="swiper-slide">
                    <div class="hero-slide">
                      <div class="hero-slide__bg hero-slide__bg--1">
                        <div class="hero-slide__bg-inner">
                          <img src="assets/img/images/home-slider-bg-1.svg" alt="">
                        </div>
                      </div>
                      <div class="hero-slide__inner">
                        <div class="hero-slide__img hero-slide__img--1">
                          <div class="hero-slide__pic">
                            <img src="assets/img/images/home-screen-1.png" alt="">
                          </div>
                        </div>
                      </div>

                      <div class="hero-slide-decor hero-slide-decor-1-1">
                        <img class="js-parallax-7" src="assets/img/images/slide-1-decor-1.svg" alt="">
                      </div>
                      <div class="hero-slide-decor hero-slide-decor-1-2">
                        <img class="js-parallax-8" src="assets/img/images/slide-1-decor-2.svg" alt="">
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="hero-slide">
                      <div class="hero-slide__bg hero-slide__bg--2">
                        <div class="hero-slide__bg-inner">
                          <img src="assets/img/images/home-slider-bg-2.svg" alt="">
                        </div>
                      </div>
                      <div class="hero-slide__inner">
                        <div class="hero-slide__img hero-slide__img--2">
                          <div class="hero-slide__pic">
                            <img src="assets/img/images/home-screen-22.png" alt="" class="revers-block">
                          </div>
                        </div>
                      </div>

                      <div class="hero-slide-decor hero-slide-decor-2-1">
                        <img class="js-parallax-7" src="assets/img/images/slide-2-decor-1.svg" alt="">
                      </div>
                      <div class="hero-slide-decor hero-slide-decor-2-2">
                        <img class="js-parallax-8" src="assets/img/images/slide-2-decor-2.svg" alt="">
                      </div>

                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="hero-slide">
                      <div class="hero-slide__bg hero-slide__bg--3">
                        <div class="hero-slide__bg-inner">
                          <img src="assets/img/images/home-slider-bg-3.svg" alt="">
                        </div>
                      </div>
                      <div class="hero-slide__inner">
                        <div class="hero-slide__img hero-slide__img--3">
                          <div class="hero-slide__pic">
                            <img src="assets/img/images/home-screen-3.png" alt="">
                          </div>
                        </div>
                      </div>

                      <div class="hero-slide-decor hero-slide-decor-3-1">
                        <img class="js-parallax-7" src="assets/img/images/slide-3-decor-1.svg" alt="">
                      </div>
                      <div class="hero-slide-decor hero-slide-decor-3-2">
                        <img class="js-parallax-8" src="assets/img/images/slide-3-decor-2.svg" alt="">
                      </div>

                    </div>
                  </div>

                </div>
                <!-- <div class="hero-slider-btns">
                  <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                  <div class="swiper-button swiper-button-custom swiper-button-next"></div>
                </div> -->
              </div>

              <div class="home-label">
                <img src="assets/img/icons/screen-label.svg" alt="">
              </div>
            </div>

            <div class="home-box">
              <div class="home-box__inner">
                <img class="home-box__decor" src="/assets/img/images/box-decor.svg" alt="">

                <div class="box-elem">
                  <div class="box-elem__inner" id="trigger-start">
                    <img src="/assets/img/images/box-11.png" alt="">
                    <canvas id="box-canvas"></canvas>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>

      <section class="home-about" id="second-section">
        <div class="container">
          <div class="home-about__content">
            <div class="home-about-content">
              <div class="section-title section-title--sm">
                Витажиналь<sup>®</sup> – <br> 5 элементов для <br> новой жизни
                <small>и ничего лишнего</small>
              </div>

              <ul class="home-about-content__list">
                <li>пять базовых микронутриентов для здоровья мамы и малыша</li>
                <li>физиологические дозы компонентов</li>
                <li>рациональное сочетание микронутриентов</li>
              </ul>

              <a href="/about" class="btn btn-fill-blue home-about-content__btn hide-tablet">
                <span class="btn__inner">
                  <span class="btn__text">О комплексе</span>
                </span>
              </a>
            </div>

            <div class="home-about-rewards">
              <a href="/mark.pdf">
                <img src="/assets/img/icons/mark.svg" alt="" class="home-about-mark">
              </a>
              <span class="home-about-reward">
                <img src="/assets/img/icons/france.svg" alt="">
                <span>французское качество</span>
              </span>
            </div>

            <div class="home-about-box">
              <div class="home-about-box__inner">
                <div class="home-about-tooltip-pgn"></div>
                <img src="/assets/img/images/box-bg-3.svg" alt="">
              </div>

              <div class="box-elem" id="trigger-stop">
                <div class="box-elem__inner">
                  <img src="/assets/img/images/box.png" alt="">
                </div>
              </div>
            </div>

            <div class="home-about-tooltips">
              <div class="home-about-tooltips__bg">
                <div class="home-about-tooltips__bg-inner">
                  <img src="/assets/img/images/tooltip-bg.svg" alt="" class="home-about-tooltips__bg-pic home-about-tooltips__bg-pic--desk">
                  <img src="/assets/img/images/tooltip-bg-2.svg" alt="" class="home-about-tooltips__bg-pic home-about-tooltips__bg-pic--mob">
                  <!-- <img src="/assets/img/images/dd.svg" alt="" class="home-about-tooltips__bg-decor"> -->
                </div>
              </div>
              <div class="home-about-tooltips__inner">
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <div class="swiper-slide">
                      <div class="home-about-tooltip" data-name="Ω" data-num="3" data-small="Омега-3">
                        <strong>Омега-3</strong>
                        <span>200 мг</span>
                        <p>
                          Необходима для подготовки к беременности, увеличивает вероятность зачатия и способствует формированию сосудов плаценты, если беременность состоялась.
                        </p>
                        <a href="/about">Подробнее</a>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div class="home-about-tooltip" data-name="D" data-num="3" data-small="витамин">
                        <strong>Витамин D</strong>
                        <span>200 МЕ (5 мкг)</span>
                        <p>
                          Участвует в правильной работе репродуктивной системы. Регулирует обмен кальция и фосфора в организме.
                        </p>
                        <a href="/about">Подробнее</a>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div class="home-about-tooltip" data-name="Е"  data-small="витамин">
                        <strong>Витамин E</strong>
                        <span>12 мг</span>
                        <p>
                          Антиоксидант, участвует в синтезе гормонов. Способствует созреванию яйцеклеток, без которых зачатие невозможно. Защищает Омега-3 от окисления.
                        </p>
                        <a href="/about">Подробнее</a>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div class="home-about-tooltip" data-name="I" data-small="йод">
                        <strong>Йод</strong>
                        <span>150 мкг</span>
                        <p>
                          Важен для нормальной работы щитовидной железы, ответственной за процессы зачатия, беременности и родов.
                        </p>
                        <a href="/about">Подробнее</a>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div class="home-about-tooltip" data-name="B" data-num="9" data-small="фолиевая кислота">
                        <strong>В9</strong>
                        <span>400 мкг (0,4 мг)</span>
                        <p>
                          Снижает риск развития врожденных пороков и дефектов нервной системы ребенка. Важно начать ее прием еще до зачатия.
                        </p>
                        <a href="/about">Подробнее</a>
                      </div>
                    </div>

                  </div>
                  <div class="home-about-tooltips__controls">
                    <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                    <div class="swiper-button swiper-button-custom swiper-button-next"></div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>

      <section class="home-screen home-screen-1" id="home-screen-1">
        <div class="container home-screen__container">
          <div class="home-screen-bg home-screen-bg--1">
            <div class="home-screen-bg__inner" data-parallax='{"y": -250}'>
              <img src="/assets/img/images/home-screen-1.png" alt="">
            </div>
          </div>
          <div class="home-screen__content">
            <div class="home-screen-content">
              <div class="tag tag--lg">Планирование беременности</div>
              <div class="section-title">
                Хочу стать <br> мамой
              </div>
              <div class="home-screen__desc">
                Правильная подготовка к материнству – залог успеха в желанной беременности и рождении здорового малыша. Витажиналь<sup>&reg;</sup>  расскажет, как сделать это правильно.
              </div>
              <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="btn btn-fill-blue home-screen-content__btn hide-tablet">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
              <a href="/become-mom-calculator" class="btn btn-fill-pink home-screen-content__btn">
                  Калькулятор овуляции
                </a>
            </div>

            <div class="home-screen-slider">
              <div class="swiper-container">
                <div class="swiper-wrapper">

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-1.svg" alt="">
                          </div>
                          <div class="common-card__title">Омега-3</div>
                          <div class="common-card__desc">
                            Необходима для подготовки к беременности, увеличивает вероятность зачатия и способствует формированию сосудов плаценты, если беременность состоялась.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-5.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин D</div>
                          <div class="common-card__desc">
                            Участвует в правильной работе репродуктивной системы. Регулирует обмен кальция и фосфора в организме.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-4.svg" alt="">
                          </div>
                          <div class="common-card__title">Фолиевая кислота</div>
                          <div class="common-card__desc">
                            Снижает риск развития врожденных пороков и дефектов нервной системы ребенка. Важно начать ее прием еще до зачатия.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-2.svg" alt="">
                          </div>
                          <div class="common-card__title">Йод</div>
                          <div class="common-card__desc">
                            Важен для нормальной работы щитовидной железы, ответственной за процессы зачатия, беременности и родов.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                   <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-3.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин Е</div>
                          <div class="common-card__desc">
                            Антиоксидант, участвует в синтезе гормонов. Способствует созреванию яйцеклеток, без которых зачатие невозможно. Защищает Омега-3 от окисления.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  

                </div>
                <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                <div class="swiper-button swiper-button-custom swiper-button-next"></div>
              </div>
            </div>

            <div class="home-screen__actions show-tablet">
              <a href="/about" class="btn btn-fill-blue home-screen-content__btn">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
            </div>
          </div>
          <div class="home-screen-label">
            <img src="/assets/img/icons/screen-label-1.svg" alt="">
          </div>
        </div>
      </section>

      <section class="home-screen home-screen-2" id="home-screen-2">
        <div class="container home-screen__container">
          <div class="home-screen-bg home-screen-bg--2">
            <div class="home-screen-bg__inner" data-parallax='{"y": -550}'>
              <img src="/assets/img/images/home-screen-22.png" alt="">
            </div>
          </div>
          <div class="home-screen__content">
            <div class="home-screen-content">
              <div class="tag tag--lg tag--white">Беременность</div>
              <div class="section-title">
                Ждем <br> малыша
              </div>
              <div class="home-screen__desc">
                Беременность – сложный этап в жизни женщины, требующий особого подхода. Витажиналь<sup>&reg;</sup> расскажет, как пройти этот путь легко и непринужденно.
              </div>
              <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="btn btn-fill-blue home-screen-content__btn hide-tablet">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
              <a href="/calculator_pregnancy" class="btn btn-fill-pink home-screen-content__btn">
                  Калькулятор беременности
              </a>
            </div>

            <div class="home-screen-slider">
              <div class="swiper-container">
                <div class="swiper-wrapper">

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-6.svg" alt="">
                          </div>
                          <div class="common-card__title">Омега-3</div>
                          <div class="common-card__desc">
                            Важна для успешного течения беременности. Способствует снижению риска преждевременных родов. Поддерживает иммунитет и оказывает и антиоксидантное действие.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-8.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин D</div>
                          <div class="common-card__desc">
                            Поддерживает баланс кальция и фосфора в организме матери, регулирует их поступление через плаценту к малышу.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-9.svg" alt="">
                          </div>
                          <div class="common-card__title">Фолиевая кислота</div>
                          <div class="common-card__desc">
                            «Критический» нутриент в период беременности. Регулирует обмен веществ в организме мамы.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-10.svg" alt="">
                          </div>
                          <div class="common-card__title">Йод</div>
                          <div class="common-card__desc">
                            Необходим для правильной работы щитовидной железы, ответственной за процессы зачатия, беременности и родов.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-7.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин Е</div>
                          <div class="common-card__desc">
                            Антиоксидант, принимающий участие в синтезе гормонов беременности. Способствует формированию клеток крови плода.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                <div class="swiper-button swiper-button-custom swiper-button-next"></div>
              </div>
            </div>

            <div class="home-screen__actions show-tablet">
              <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="btn btn-fill-blue home-screen-content__btn">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
            </div>
          </div>
          <div class="home-screen-label">
            <img src="/assets/img/icons/screen-label-2.svg" alt="">
          </div>
        </div>
      </section>

      <section class="home-screen home-screen-3" id="home-screen-3">
        <div class="container home-screen__container">
          <div class="home-screen-bg home-screen-bg--3">
            <div class="home-screen-bg__inner" data-parallax='{"y": -300}'>
              <img src="/assets/img/images/home-screen-3.png" alt="">
            </div>
          </div>
          <div class="home-screen__content">
            <div class="home-screen-content">
              <div class="tag tag--lg">Развитие малыша</div>
              <div class="section-title">
                Мама, я <br> родился
              </div>
              <div class="home-screen__desc">
                Вот и настал счастливый момент. Вы — мама! Витажиналь<sup>&reg;</sup> расскажет, как сделать период лактации комфортным для себя и малыша.
              </div>
              <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="btn btn-fill-blue home-screen-content__btn hide-tablet">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
              <a href="/calculator_height-weight" class="btn btn-fill-pink home-screen-content__btn">
                  Калькулятор роста и веса
                </a>
            </div>

            <div class="home-screen-slider">
              <div class="swiper-container">
                <div class="swiper-wrapper">

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-11.svg" alt="">
                          </div>
                          <div class="common-card__title">Фолиевая кислота</div>
                          <div class="common-card__desc">
                            Очень важна в период лактации. Способствует насыщению фолатами молока и оптимальному процессу развития организма новорожденного
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-12.svg" alt="">
                          </div>
                          <div class="common-card__title">Омега-3</div>
                          <div class="common-card__desc">
                            Не синтезируется в организме самостоятельно. Важна для созревания головного мозга и сетчатки глаза. Способствует улучшению умственных способностей ребенка.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-14.svg" alt="">
                          </div>
                          <div class="common-card__title">Йод</div>
                          <div class="common-card__desc">
                            Помогает поддержать и продлить грудное вскармливание. Дефицит йода может отражаться на умственных способностях.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-15.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин D</div>
                          <div class="common-card__desc">
                            Способствует нормальной лактации. Отвечает за укрепление костей, деятельность нервной системы, стимулирует иммунитет мамы и малыша.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="common-card">
                      <div class="common-card__inner">
                        <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="common-card__body">
                          <div class="common-card__logo">
                            <img src="/assets/img/icons/common-card-logo-13.svg" alt="">
                          </div>
                          <div class="common-card__title">Витамин Е</div>
                          <div class="common-card__desc">
                            Положительно влияет на функции скелетной мускулатуры, сердца, сосудов и половых желез. Участвует в процессе роста клеток, производстве белков и гемоглобина.
                          </div>
                        </a>
                        <div class="common-card__footer">
                          <!--- <a href="([^"]+)" class="common-card__link">Читать подробнее</a> -->
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                <div class="swiper-button swiper-button-custom swiper-button-next"></div>
              </div>
            </div>

            <div class="home-screen__actions show-tablet">
              <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="btn btn-fill-blue home-screen-content__btn">
                <span class="btn__inner">
                  <span class="btn__text">Подробнее</span>
                </span>
              </a>
            </div>
          </div>
          <div class="home-screen-label">
            <img src="/assets/img/icons/screen-label-3.svg" alt="">
          </div>
        </div>
      </section>

      <section class="useful-articles home-useful-articles">
        <div class="container">

          <div class="home-useful-articles__content">
            <div class="section-small-title">
              Полезные статьи
            </div>

            <div class="useful-articles-slider hide-phone-lg">
              <div class="swiper-container">
                <div class="swiper-wrapper">

                  @foreach ($articles as $a)
                  <div class="swiper-slide">
                    <div class="article-card">
                      <div class="article-card__inner">
                        <div class="article-card__top">
                          <div class="article-card__img">
                            <img src="/{{ $a->small_banner }}" alt="">
                          </div>
                        </div>
                        <div class="article-card__body">
                          <div class="article-card__tags">
                            @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                            @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                            @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                          </div>
                          <h4 class="article-card__title">{!! $a->title !!}</h4>
                          <div class="article-card__desc">
                            {{ $a->tiser }}
                          </div>
                          <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach

                </div>
                <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
                <div class="swiper-button swiper-button-custom swiper-button-next"></div>
              </div>
            </div>

            <div class="useful-articles-cards show-phone-lg">
              @foreach ($articles->slice(0, 3) as $a)

              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                     @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
              @endforeach
            </div>

            <div class="useful-articles__actions">
              <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
            </div>
          </div>

        </div>
      </section>

      <section class="order-block white-wrapper" id="pay">
        <div class="container">
          <div class="section-small-title">
            Где купить Витажиналь<sup>&reg;</sup>
          </div>

          <div class="pharmacies" id="pharmacies">
            <div class="pharmacies__row">
              @foreach ($pharmacies as $pharmacy)
              <div class="pharmacies__col">
                <a @if ($pharmacy->link)href="{{ $pharmacy->link }}"@endif class="pharmacies-card">
                  <div class="pharmacies-card__inner">
                    <img src="/{{ $pharmacy->logo }}" alt="">
                  </div>
                </a>
              </div>
              @endforeach
            </div>
          </div>

          <div class="order-block__actions">
            <a href="#" class="btn btn-fill-pink js-show-all" data-target="#pharmacies">
              <span class="btn__inner">
                <span class="btn__text">
                  <span class="btn__toggle">
                    <span class="btn__toggle-unactive">Показать все аптеки</span>
                    <span class="btn__toggle-active">Скрыть</span>
                  </span>
                </span>
              </span>
            </a>
          </div>


          <div class="uses-acco js-acco">
            <div class="uses-acco-item js-acco-item">
              <div class="uses-acco-item__head js-acco-head">
                <span class="uses-acco-item__ico">
                  <img src="/assets/img/icons/acco-ico-1.svg" alt="">
                </span>
                <span><u>Список используемой литературы</u></span>
                <span class="btn btn-fill-pink-light">Смотреть </span>
              </div>
              <div class="uses-acco-item__body js-acco-body">
                <div class="uses-acco-item__content">
                  {!! $page->literature !!}
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

    </main>
@endsection