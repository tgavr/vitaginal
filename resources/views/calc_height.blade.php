@extends('layouts.site')

@section('seo')
<title>Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша</title>
<meta name="keywords" content="">
<meta name="description" content="Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша">
@endsection

@section('css')
<link rel="stylesheet" href="/assets/libs/air-datepicker/css/datepicker.min.css">
<link rel="stylesheet" href="/assets/libs/fullcalendar-5.5.1/main.min.css">
@endsection

@section('content')
<main class="main offset-header">
  <article class="article">
    <div class="container">
      <div class="article__inner">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha">Хочу стать мамой</a>
            </li>
            <li>
              <a href="/calculator_height-weight">Калькулятор роста и веса</a>
            </li>
          </ul>
        </div>

        <h1>Калькулятор роста и веса <br>(по данным ВОЗ)</h1>

        <div class="calcualator calcualator--hw"> <!--toggle clsass .result -->
          <form class="calcualator-wheight-form" action="/calculator_height-weight">
            <div class="calcualator__step calcualator__step--1" data-step="1" @if($gender) style="display: none;" @endif>
              <div class="calcualator__step-row">
                <label class="calcualator__field">
                  <img src="/assets/img/images/calcualator/boy.svg" alt="">
                  <input type="radio" name="gender" value="boy" @if($gender == "boy") checked @endif >
                  <b>Мальчик</b>
                </label>
                <label class="calcualator__field">
                  <img src="/assets/img/images/calcualator/girl.svg" alt="">
                  <input type="radio" name="gender" value="girl" @if($gender == "girl") checked @endif>
                  <b>Девочка</b>
                </label>
              </div>
              <b>Выберите одно из двух </b>
            </div>




            <div class="calcualator__step calcualator__step--2" data-step="boy" @if(!$gender || $gender == "girl") style="display: none;" @endif>
              <div class="calcualator__step-row">
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>Рост</b>
                    <div class="calcualator-input">
                      <input class="calcualator-input-height" type="number" placeholder="101" step="0.1" value="{{ $height }}" @if($gender && $gender == "boy") name="height" @endif>
                      <span class="calcualator-input__units">см</span>
                    </div>
                  </div>
                </div>
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>Вес</b>
                    <div class="calcualator-input">
                      <input class="calcualator-input-weight" type="number" placeholder="15" step="0.1" value="{{ $weight }}" @if($gender && $gender == "boy") name="weight" @endif>
                      <span class="calcualator-input__units">кг</span>
                    </div>
                  </div>
                </div>
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>День рождения мальчика</b>
                    <div class="calendar-form">
                      <input class="js-calendar-input calcualator-input-birddate" type="text" placeholder="__.__.____" @if($gender && $gender == "boy") name="bird_date" value="{{ $bird_date }}" @endif>
                      <button type="button" class="btn btn-fill-pink-light-2 js-calendar">Календарь</button>
                      <div class="datepicker-here"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="calcualator__step-actions">
                <a href="#" class="btn btn-border-pink-light calcualator__step-action-btn calcualator__step-action-btn--back js-prev" data-go-to="1">
                  Назад
                </a>
                <button type="submit" class="btn btn-fill-blue calcualator__step-action-btn">
                  Рассчитать
                </button>
              </div>
            </div>

            <div class="calcualator__step calcualator__step--2" data-step="girl" @if(!$gender || $gender == "boy") style="display: none;" @endif>
              <div class="calcualator__step-row">
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>Рост</b>
                    <div class="calcualator-input">
                      <input class="calcualator-input-height" type="number" placeholder="101" type="number" step="0.1" value="{{ $height }}" @if($gender && $gender == "girl") name="height"  @endif>
                      <span class="calcualator-input__units">см</span>
                    </div>
                  </div>
                </div>
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>Вес</b>
                    <div class="calcualator-input">
                      <input class="calcualator-input-weight" type="number" placeholder="15" type="number" step="0.1" value="{{ $weight }}" @if($gender && $gender == "girl") name="weight" @endif>
                      <span class="calcualator-input__units">кг</span>
                    </div>
                  </div>
                </div>
                <div class="calcualator__step-col">
                  <div class="calcualator__field">
                    <b>День рождения девочки</b>
                    <div class="calendar-form">
                      <input class="js-calendar-input calcualator-input-birddate" type="text" placeholder="__.__.____" @if($gender && $gender == "girl") name="bird_date" value="{{ $bird_date }}" @endif>
                      <button type="button" class="btn btn-fill-pink-light-2 js-calendar">Календарь</button>
                      <div class="datepicker-here"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="calcualator__step-actions">
                <a href="#" class="btn btn-border-pink-light calcualator__step-action-btn calcualator__step-action-btn--back js-prev" data-go-to="1">
                  Назад
                </a>
                <button type="submit" class="btn btn-fill-blue calcualator__step-action-btn">
                  Рассчитать
                </button>
              </div>
            </div>



            @if ($word)
            <script type="text/javascript">
              var VARresult = true;
            </script>
        
            <div class="calcualator__result calcualator__result--{{ $gender }}" data-step="result-1" >
              <div class="calcualator__result-hw-child">
                <img src="/assets/img/images/calcualator/{{ $gender }}.svg" alt="">
                {{ $word }} сегодня {{ $y }}, <br> {{ $m }} и {{ $d }}
              </div>
              
              <div class="row">
                <div class="col">
                  <div class="calcualator__result-hw-status">
                    <img src="/assets/img/images/calcualator/status-{{ $values['height']['color'] }}.svg" alt="">
                    <b>Рост {{ $values['height']['state'] }}</b><br>
                    <small>{{ $values['height']['state2'] }}</small>
                  </div>
                </div>
                <div class="col">
                  <div class="calcualator__result-hw-status">
                    <img src="/assets/img/images/calcualator/status-{{ $values['weight']['color'] }}.svg" alt="">
                    <b>Вес {{ $values['weight']['state'] }}</b><br>
                    <small>{{ $values['weight']['state2'] }}</small>
                  </div>
                </div>
                <div class="col">
                  <div class="calcualator__result-hw-status">
                    <img src="/assets/img/images/calcualator/status-{{ $values['imt']['color'] }}.svg" alt="">
                    <b>ИМТ {{ $values['imt']['state'] }}</b> <br>
                    <small><b>{{ $values['imt']['state2'] }}</b></small>
                  </div>
                </div>
              </div>
              @if ($values['imt']['text'])
              <div class="calcualator__field calcualator__result-desc">
                <p><b>ИМТ {{ $values['imt']['state'] }} {{ $values['imt']['value'] }} кг/м2  (средний от {{ $values['imt']['min'] }} до {{ $values['imt']['max'] }} кг/м2)</b></p>
                <p>{{ $values['imt']['text'] }}</p>
              </div>
              @endif
              
            </div>
            @endif

            
          </form>
        </div>


        <div class="article__content" style="border: solid #ffe2ec 4px; padding-top: 1rem !important; padding-bottom: 1rem; margin-top: 3rem;">
          <!-- <h2>Как оценить рост, вес и индекс массы тела (ИМТ)?</h2> -->
         
          <p><strong>Калькулятор учитывает такие показатели как вес, рост, возраст и ИМТ и дает полную оценку.</strong> Программа поможет оценить, как идет развитие ребенка согласно возраста, выявить проблемы и возможные риски развития проблем. В случае значительных отклонений в росте и весе необходимо обратиться к специалистам.</p>
        </div>
        
        
      </div>
    </div>
  </article>

  <div class="useful-articles pink-wrapper">
    <div class="container">

      <div class="section-small-title">
        Полезные статьи
      </div>

      <div class="useful-articles-slider hide-phone-lg">
        <div class="swiper-container">
          <div class="swiper-wrapper">

            @foreach ($articles as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($articles->slice(0, 3) as $a)

        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>

      <div class="uses-acco js-acco uses-acco" style="margin-top: 5.9375rem">
        <div class="uses-acco-item js-acco-item">
          <div class="uses-acco-item__head js-acco-head">
            <span class="uses-acco-item__ico">
              <img src="/assets/img/icons/acco-ico-1.svg" alt="">
            </span>
            <span><u>Список используемой литературы</u></span>
            <span class="btn btn-fill-pink-light">Смотреть </span>
          </div>
          <div class="uses-acco-item__body js-acco-body">
            <div class="uses-acco-item__content">
              {!! $page->literature !!}
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</main>
@endsection


@section('js')
<script src="/assets/libs/jquery.inputmask.min.js"></script>
<script src="/assets/libs/air-datepicker/js/datepicker.min.js"></script>
<script src="/assets/libs/moment.min.js"></script>
<script src="/assets/libs/fullcalendar-5.5.1/main.min.js"></script>
<script src="/assets/libs/fullcalendar-5.5.1/main.global.min.js"></script>
@endsection