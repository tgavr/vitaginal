@extends('layouts.site')

@section('seo')
<title>Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша</title>
<meta name="keywords" content="">
<meta name="description" content="Витажиналь® – базовый комплекс микронутриентов для здоровья мамы и малыша">
@endsection

@section('css')
<link rel="stylesheet" href="/assets/libs/air-datepicker/css/datepicker.min.css">
<link rel="stylesheet" href="/assets/libs/fullcalendar-5.5.1/main.min.css">
@endsection

@section('content')

<main class="main offset-header">
  <article class="article">
    <div class="container">
      <div class="article__inner">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a href="/hochu-stat-mamoj-5-kriteriev-uspekha">Хочу стать мамой</a>
            </li>
            <li>
              <a href="/become-mom-calculator">Калькулятор овуляции</a>
            </li>
          </ul>
        </div>

        <h1>Калькулятор овуляции</h1>

        <div class="become-mom-calculator-desc">
          При планировании беременности важно знать дату овуляции. Точно и легко определить эту дату поможет калькулятор овуляции. Введите данные о вашем менструальном цикле - в календаре будут вычислены и отмечены дни менструации, овуляции и наиболее благоприятные дни для зачатия (фертильные дни).
        </div>

        <div class="calcualator calcualator--become-mom become-mom-calcualator"> <!--toggle clsass .result -->

          <div class="calcualator__step calcualator__step--1">
            <form class="become-mom-calcualator__form js-become-mom-form">
              <div class="become-mom-layout">
                <div class="become-mom-layout__inner">
                  <div class="become-mom-layout__col">
                    <div class="become-mom-layout__col-row">
                      <div class="become-mom-layout__field">
                        <div class="become-mom-layout__label" data-num="1">
                          <span class="become-mom-layout__field-text">
                            Продолжительность цикла
                          </span>
                          <div class="counter-el">
                            <div class="counter-el__display">
                              <input class="counter-el__control" data-min="22" data-max="35" name="c-duration" type="text" placeholder="0">
                            </div>
                            <div class="counter-el__controls">
                              <button class="counter-el__btn counter-el__btn--decr" aria-label="decrement-button"></button>
                              <button class="counter-el__btn counter-el__btn--incr" aria-label="increment-button"></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="become-mom-layout__col-row">
                      <div class="become-mom-layout__field">
                        <div class="become-mom-layout__label"  data-num="2">
                          <span class="become-mom-layout__field-text">
                            Длительность менструации
                          </span>
                          <div class="counter-el">
                            <div class="counter-el__display">
                              <input class="counter-el__control" data-min="2" data-max="7" name="m-duration" type="text" placeholder="0">
                            </div>
                            <div class="counter-el__controls">
                              <button class="counter-el__btn counter-el__btn--decr" aria-label="decrement-button"></button>
                              <button class="counter-el__btn counter-el__btn--incr" aria-label="increment-button"></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="become-mom-layout__col">
                    <div class="become-mom-layout__col-row">
                      <div class="become-mom-layout__field">
                        <div class="become-mom-layout__label" data-num="3">
                          <span class="become-mom-layout__field-text">
                            Первый день последнего цикла
                          </span>
                        </div>
                        <div class="calendar-form">
                          <input class="js-calendar-input" name="c-first-day" type="text" placeholder="__.__.____" inputmode="text">
                          <button type="button" class="btn btn-fill-pink-light-2 js-calendar">Календарь</button>
                          <div class="datepicker-here"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="become-mom-calculator__info">
                При нерегулярном цикле онлайн-калькуляторы овуляции применять нельзя. В этом случае для расчета овуляции используйте другие методы (УЗИ, изменение базальной температуры, тесты на овуляцию).
              </div>

              <button type="submit" class="btn btn-fill-blue">
                Рассчитать
              </button>
            </form>
          </div>

          <div class="calcualator__result become-mom-calculator__result" style="display: none;">
            <h4 class="become-mom-calculator__result-title">
              Ваш календарь планирования беременности
            </h4>
            <div class="become-mom-calculator__calendar" id='calendar-result'></div>

            <div class="become-mom-calculator__legend">
              <div class="become-mom-calculator__legend-item">
                <span class="blue">2</span>
                Фертильный день
              </div>
              <div class="become-mom-calculator__legend-item">
                <span class="pink">
                  <img src="/assets/img/icons/cell.svg" alt="">
                </span>
                Овуляция
              </div>
              <div class="become-mom-calculator__legend-item">
                <span class="light">
                  <img src="/assets/img/icons/drop.svg" alt="">
                </span>
                Менструация
              </div>
            </div>
          </div>

        </div>

        <div class="article__content">
          <h2>Рассчитываем день овуляции с помощью калькулятора</h2>

          <p>
            Понимание закономерности и цикличности работы своего организма открывает перед женщиной самые широкие возможности – она может выстраивать индивидуальный план контрацепции, а может более эффективно планировать зачатие ребенка. И в этих вопросах без умения точно определять день овуляции не обойтись.
          </p>
          <p>
            Принцип работы калькулятора основан на календарном методе определения женской плодности. По календарному методу день овуляции приходится ровно на середину цикла и условно разделяет его на две фазы – фазу созревания фолликула (фолликулярную) и фазу после разрыва фолликула (лютеиновую). Определение срока овуляции календарным способом при помощи калькулятора не представляет большого труда.
          </p>

          <strong>
            Для этого нужно знать следующее:
          </strong>

          <ul>
            <li>
              <strong>длительность своего менструального цикла</strong> (период времени от первого дня месячных до первого дня последующих месячных);
            </li>
            <li>
              <strong>длительность самого менструального периода</strong> (от начала до окончания менструальных выделений).
            </li>
          </ul>

          <p>
            Это нужно в основном для оценки фертильности, чтобы знать, достаточна ли фолликулярная фаза цикла по времени. <strong>Рассчитать по календарю овуляцию можно по формуле</strong> D-14, где D – длительность вашего цикла, а 14 – это стандартная длительность второй, лютеиновой фазы менструального цикла. Дело в том, что первая фаза у женщин может отличаться по длительности, причем разительно. А вот вторая обычно у дам с разной продолжительностью женского цикла почти одинакова, и составляет она 14 дней (плюс-минус сутки). Различные обстоятельства и неблагоприятные факторы на вторую фазу и ее длительность имеют меньшее влияние, поэтому ее колебания обычно весьма незначительны.
          </p>

          <!-- <div class="uses-acco js-acco">
            <div class="uses-acco-item js-acco-item">
              <div class="uses-acco-item__head js-acco-head">
                <span class="uses-acco-item__ico">
                  <img src="/assets/img/icons/acco-ico-1.svg" alt="">
                </span>
                <span><u>Список используемой литературы</u></span>
                <span class="btn btn-fill-pink-light">Смотреть</span>
              </div>
              <div class="uses-acco-item__body js-acco-body">
                <div class="uses-acco-item__content">
                  <ul>
                    <li>
                       <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                    </li>
                    <li>
                       <strong>Василевский И.В.</strong> Современные возможности организации питания детей первого года жизни. Педиатрия. Восточная Европа. 2014. № 1 (05). С. 157-165.
                    </li>
                    <li>
                       <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                    </li>
                    <li>
                       <strong>Василевский И.В.</strong> Современные возможности организации питания детей первого года жизни. Педиатрия. Восточная Европа. 2014. № 1 (05). С. 157-165.
                    </li>
                    <li>
                       <strong>Ших Е.В., Бриль Ю.А.</strong> Каждой - по потребностям! От обезличенной витаминопрофилактики - к персонализированной нутритивной поддержке. <strong>StatusPraesens. Гинекология, акушерство, бесплодный брак. 2019. № 1 (54). С. 59-65.</strong>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->

        </div>

      </div>
    </div>
  </article>

  <div class="useful-articles pink-wrapper">
    <div class="container">

      <div class="section-small-title">
        Полезные статьи
      </div>

      <div class="useful-articles-slider hide-phone-lg">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            @foreach ($articles as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($articles->slice(0, 3) as $a)

        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>

    </div>
  </div>
</main>
@endsection


@section('js')
<script src="/assets/libs/jquery.inputmask.min.js"></script>
<script src="/assets/libs/air-datepicker/js/datepicker.min.js"></script>
<script src="/assets/libs/moment.min.js"></script>
<script src="/assets/libs/fullcalendar-5.5.1/main.min.js"></script>
<script src="/assets/libs/fullcalendar-5.5.1/main.global.min.js"></script>
@endsection