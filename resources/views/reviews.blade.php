@extends('layouts.site')

@section('seo')
<title>Инструкция по применению препарата Витажиналь</title>
<meta name="keywords" content="">
<meta name="description" content="">
@endsection

@section('content')
<main class="main offset-header">
  <article class="article article3">
    <div class="container">
      <div class="article__inner">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a href="#">Отзывы</a>
            </li>
          </ul>
        </div>

        <h1>Отзывы <br>покупателей</h1>
        <div class="reviews-block">
          <div class="reviews-block__row">
            @foreach ($reviews as $a => $review)
            <div class="reviews-block__col @if($a > 7) hide @endif" >
              <div class="review-card">
                <div class="review-card__name">{{ $review->name }}</div>
                <div class="review-card__stars">
                  @for ($i = 1; $i <= $review->stars; $i++)
                  <img src="assets/img/icons/star.svg" alt="">
                  @endfor
                </div>
                <div class="review-card__text">{{ $review->text }}</div>
                <a href="{{ $review->link }}" class="review-card__link">Читать на <span>{{ $review->site }}</span></a>
              </div>
            </div>
            @endforeach
          </div>
          <button class="btn btn-fill-pink reviews-block__btn">Читать все отзывы</button>
        </div>


      </div>
    </div>
  </article>

</main>
   
@endsection