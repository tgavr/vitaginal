@extends('layouts.site')

@section('seo')

<title>Полезные статьи от Витажиналь®</title>
<meta name="keywords" content="">
<meta name="description" content="Во время беременности организм женщины нуждается в повышенном потреблении ряда питательных веществ и микронутриентов">


<title></title>
<meta name="keywords" content="">
<meta name="description" content="">
@endsection

@section('content')
<main class="main offset-header">
  <div class="articles">
    <div class="articles__head">
      <div class="container">
        <div class="breadcrumbs">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a>Статьи</a>
            </li>
          </ul>
        </div>
        <h1 class="main-title">Статьи</h1>

        <ul class="articles-filters">
          <li>
            <a href="/articles" @if($stage == 0)class="active"@endif>Все статьи</a>
          </li>
          <li>
            <a href="/articles?stage=1" @if($stage == 1)class="active"@endif>Хочу стать мамой</a>
          </li>
          
          <li>
            <a href="/articles?stage=2" @if($stage == 2)class="active"@endif>Беременность</a>
          </li>
          <li>
            <a href="/articles?stage=3" @if($stage == 3)class="active"@endif>Развитие малыша</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="articles__list">
      <div class="container">
        <div class="articles__row">
          @foreach ($articles as $a)
          <div class="articles__col">
            <div class="article-card">
              <div class="article-card__inner">
                <div class="article-card__top">
                  <div class="article-card__img">
                    <img src="/{{ $a->small_banner }}" alt="">
                  </div>
                </div>
                <div class="article-card__body">
                  <div class="article-card__tags">
                    @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                    @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                    @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                  </div>
                  <h4 class="article-card__title">{!! $a->title !!}</h4>
                  <div class="article-card__desc">
                    {{ $a->tiser }}
                  </div>
                  <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  
</main>
@endsection