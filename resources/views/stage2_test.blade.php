@extends('layouts.site')

@section('seo')
<title>{{ $page->title }}</title>
<meta name="keywords" content="">
<meta name="description" content="{{ $page->description }}">
@endsection

@section('content')
<!-- main -->
<main class="main">
  <section class="step-hero step-hero--waiting-baby">
    <div class="container">
      <div class="step-hero__content offset-header">
        <div class="breadcrumbs breadcrumbs--invert">
          <ul>
            <li>
              <a href="/">Главная</a>
            </li>
            <li>
              <a >Ждем малыша</a>
            </li>
          </ul>
        </div>
        <h1 class="main-title">Ждем малыша</h1>
        <div class="step-hero__desc">
          <p>Беременность – сложный этап в жизни женщины, требующий особого подхода. 
Витажиналь<sup>®</sup><br>
расскажет, как пройти этот путь легко и непринужденно.
</p>
        </div>
        <a href="#to-next" class="btn btn-fill-pink step-hero__down-link scroll-to">Подробнее</a>
      </div>
      <img class="step-hero__emblem" src="/assets/img/icons/screen-label-2.svg" alt="">
    </div>
  </section>
  <section class="elements-life elements-life--waiting-baby" id="to-next">
    <div class="container">
      <div class="elements-life__cover">
        <div class="elements-life__elements elements-life__elements--waiting-baby">
          <div class="elements-life__bib" data-parallax='{"y": -150}'>
            <img class="js-parallax-1" src="/assets/img/images/elements-life/bib.svg" alt="">
          </div>
          <div class="elements-life__beanbag" data-parallax='{"y": -150}'>
            <div>
              <img class="js-parallax-2"  src="/assets/img/images/elements-life/beanbag.svg" alt="">
            </div>
          </div>
          <div class="elements-life__nipple" data-parallax='{"y": -150}'>
            <img class="js-parallax-3"  src="/assets/img/images/elements-life/nipple.svg" alt="">
          </div>
          <img class="elements-life__product" src="/assets/img/images/elements-life/product.png" alt="">
          <img class="elements-life__reward" src="/assets/img/images/elements-life/reward.svg" alt="">
        </div>
        <div class="elements-life__content">
          <h2 class="main-title">Витажиналь<sup>&reg;</sup> – <br>5 элементов для новой жизни <span class="elements-life__subtitle">и ничего лишнего</span></h2>
          <div class="elements-life__properties-row">
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico elements-properties__ico--kubs">
                  <img src="/assets/img/images/elements-life/kubs.svg" alt="">
                </div>
                <div>Физиологические дозы компонентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/micronutrients.svg" alt="">
                </div>
                <div>Рациональное сочетание микронутриентов</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/five.svg" alt="">
                </div>
                <div>Пять базовых микронутриентов для здоровья мамы и малыша</div>
              </div> 
            </div>
            <div class="elements-life__properties-col">
              <div class="elements-properties">
                <div class="elements-properties__ico">
                  <img src="/assets/img/images/elements-life/quality.svg" alt="">
                </div>
                <div>Французское <br>качество</div>
              </div> 
            </div>
          </div>

          <div class="elements-life__btns">
            <a href="/about" class="btn btn-fill-blue home-screen-content__btn">
              <span class="btn__inner">
                <span class="btn__text">О комплексе</span>
              </span>
            </a>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="mom-baby-block">
    <div class="container">

      <div class="mom-baby-block__cover">
        <div class="mom-baby-replaceable">
          <div class="mom-baby-replaceable__inner">
            <div class="swiper-container">
              <div class="swiper-wrapper">

                <div class="swiper-slide">
                  <div class="mom-baby-tooltip" data-name="B" data-num="9" data-small="Фолиевая кислота">
                    <h2 class="main-title vit-b9"><span class="main-title__ico-el">B <sub>9</sub> </span> Фолиевая <br>кислота</h2>
                    <div class="mom-baby-replaceable__row">
                      <div class="mom-baby-replaceable__col">
                        <h4>Для малыша</h4>
                        <p>Участвует в процессах роста и развитии. Способствует снижению риска развития дефектов нервной трубки.  Ее дефицит может увеличивать риск задержки умственного развития ребенка </p>
                      </div>
                      <div class="mom-baby-replaceable__col">
                        <h4>Для мамы</h4>
                        <p>«Критический» нутриент в период беременности. Регулирует обмен веществ в организме мамы </p>
                      </div>
                    </div>
                    <a class="js-slider-more mom-baby-replaceable__read-more">Читать подробнее</a>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="mom-baby-tooltip" data-name="I" data-small="Йод">
                    <h2 class="main-title vit-i"><span class="main-title__ico-el">I</span> Йод <br><br></h2>
                    <div class="mom-baby-replaceable__row">
                      <div class="mom-baby-replaceable__col">
                        <h4>Для малыша</h4>
                        <p>Недостаток йода при беременности может способствовать задержке умственного и физического развития ребенка.</p>
                      </div>
                      <div class="mom-baby-replaceable__col">
                        <h4>Для мамы</h4>
                        <p>Необходим для правильной работы щитовидной железы, ответственной за процессы зачатия, беременности и родов.</p>
                      </div>
                    </div>
                    <a class="js-slider-more mom-baby-replaceable__read-more">Читать подробнее</a>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="mom-baby-tooltip" data-name="Ω" data-num="3" data-small="Омега-3 <span><br>ДГК</span>">
                    <h2 class="main-title vit-omega3"><span class="main-title__ico-el">Ω <sub>3</sub> </span> Омега&nbsp;3<br> ДГК</h2>
                    <div class="mom-baby-replaceable__row">
                      <div class="mom-baby-replaceable__col">
                        <h4>Для малыша</h4>
                        <p>Необходима для правильного развития головного мозга и органа зрения будущего ребенка. Способствует снижению риска рождения недоношенного по весу малыша. </p>
                      </div>
                      <div class="mom-baby-replaceable__col">
                        <h4>Для мамы</h4>
                        <p>Важна для успешного течения беременности. Способствует снижению риска преждевременных родов. Поддерживает иммунитет и оказывает и антиоксидантное действие. </p>
                      </div>
                    </div>
                    <a class="js-slider-more mom-baby-replaceable__read-more">Читать подробнее</a>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="mom-baby-tooltip" data-name="D" data-num="3" data-small="Витамин&nbsp;<span>D3</span>">
                    <h2 class="main-title vit-d3"><span class="main-title__ico-el">D <sub>3</sub> </span> Витамин&nbsp;D3</h2>
                    <div class="mom-baby-replaceable__row">
                      <div class="mom-baby-replaceable__col">
                        <h4>Для малыша</h4>
                        <p>Отвечает за минерализацию костей и деятельность нервной системы, стимулирует иммунитет.</p>
                      </div>
                      <div class="mom-baby-replaceable__col">
                        <h4>Для мамы</h4>
                        <p>Поддерживает баланс кальция и фосфора в организме матери, регулирует их поступление через плаценту к малышу.</p>
                      </div>
                    </div>
                    <a class="js-slider-more mom-baby-replaceable__read-more">Читать подробнее</a>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="mom-baby-tooltip" data-name="E" data-small="Витамин&nbsp;<span>E</span>">
                    <h2 class="main-title vit-e"><span class="main-title__ico-el">E</span> Витамин&nbsp;E</h2>
                    <div class="mom-baby-replaceable__row">
                      <div class="mom-baby-replaceable__col">
                        <h4>Для малыша</h4>
                        <p>Участвует в процессах роста клеток, производстве белков (строительного материала организма) и гемоглобина. Может положительно влиять на скелетную мускулатуру, сердце, сосуды и половые железы малыша.</p>
                      </div>
                      <div class="mom-baby-replaceable__col">
                        <h4>Для мамы</h4>
                        <p>Антиоксидант, принимающий участие в синтезе гормонов беременности. Способствует формированию клеток крови плода.</p>
                      </div>
                    </div>
                    <a class="js-slider-more mom-baby-replaceable__read-more">Читать подробнее</a>
                  </div>
                </div>


              </div>
              <!-- <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
              <div class="swiper-button swiper-button-custom swiper-button-next"></div> -->
            </div>
          </div>
        </div>
        <div class="mom-baby-block__tooltip-pgn-cover">
          <div class="mom-baby-tooltip-pgn">
          </div>
        </div>
      </div>
    </div>
  </section>


  <div class="useful-articles pink-wrapper useful-articles--happy-pregnancy">
    <div class="container">

      <script async type="text/javascript" src="//asset.fwcdn2.com/js/embed-feed.js"></script>
    <fw-embed-feed
      channel="besins_channel"
playlist="gpY71v"
      mode="row"
      open_in="default"
      max_videos="0"
      placement="middle"
      player_placement="bottom-right"                       
    ></fw-embed-feed>

      <div class="section-small-title" style="margin-top: 2rem">
        5 причин счастливой <br> беременности
      </div>

      <div class="useful-articles-slider hide-phone-lg space-between-20-10">
        <div class="swiper-container">
          <div class="swiper-wrapper">

            @foreach ($articles as $a)
            <div class="swiper-slide">
              <div class="article-card">
                <div class="article-card__inner">
                  <div class="article-card__top">
                    <div class="article-card__img">
                      <img src="/{{ $a->small_banner }}" alt="">
                    </div>
                  </div>
                  <div class="article-card__body">
                    <div class="article-card__tags">
                      @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                      @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                      @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
                    </div>
                    <h4 class="article-card__title">{!! $a->title !!}</h4>
                    <div class="article-card__desc">
                      {{ $a->tiser }}
                    </div>
                    <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-button swiper-button-custom swiper-button-prev"></div>
          <div class="swiper-button swiper-button-custom swiper-button-next"></div>
        </div>
      </div>

      <div class="useful-articles-cards show-phone-lg">
        @foreach ($articles->slice(0, 3) as $a)

        <div class="article-card">
          <div class="article-card__inner">
            <div class="article-card__top">
              <div class="article-card__img">
                <img src="/{{ $a->small_banner }}" alt="">
              </div>
            </div>
            <div class="article-card__body">
              <div class="article-card__tags">
               @if($a->on_stage1)<a href="/hochu-stat-mamoj-5-kriteriev-uspekha" class="tag">Хочу стать мамой</a>@endif
                @if($a->on_stage2)<a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti" class="tag">Беременность</a>@endif
                @if($a->on_stage3)<a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha" class="tag"> Кормление малыша</a>@endif
              </div>
              <h4 class="article-card__title">{!! $a->title !!}</h4>
              <div class="article-card__desc">
                {{ $a->tiser }}
              </div>
              <a href="/articles/{{ $a->url }}" class="article-card__link"><u>Читать</u></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <div class="useful-articles__actions">
        <a href="/articles" class="btn btn-fill-pink">Все статьи</a>
      </div>

      <div class="uses-acco js-acco uses-acco">
        <div class="uses-acco-item js-acco-item">
          <div class="uses-acco-item__head js-acco-head">
            <span class="uses-acco-item__ico">
              <img src="/assets/img/icons/acco-ico-1.svg" alt="">
            </span>
            <span><u>Список используемой литературы</u></span>
            <span class="btn btn-fill-pink-light">Смотреть </span>
          </div>
          <div class="uses-acco-item__body js-acco-body">
            <div class="uses-acco-item__content">
              {!! $page->literature !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</main>
<!-- /main -->
@endsection