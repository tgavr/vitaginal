<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">

  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="cleartype" content="on" />
  <meta http-equiv="imagetoolbar" content="no" />
  <meta http-equiv="msthemecompatible" content="no" />

  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

  <meta name="format-detection" content="telephone=no" />
  <meta name="format-detection" content="address=no" />

  <meta name="keywords" content="">
  <meta name="description" content="">

  
  <link rel="icon" href="/assets/img/favicon.svg">

  <link rel="preload" href="/assets/libs/fancybox/jquery.fancybox.min.css" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="/assets/libs/swiper/swiper.min.css" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="/assets/css/critical.css" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="/assets/css/fonts.css" as="style" onload="this.rel='stylesheet'">
  <link rel="stylesheet" href="/assets/css/styles.css">

<script type="text/javascript">!function(){var
t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src='https://vk.com/
js/api/openapi.js?169',t.onload=function(){VK.Retargeting.Init("VK-RTRG-332217-
3hMD0"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img
src="https://vk.com/rtrg?p=VK-RTRG-332217-3hMD0" style="position:fixed; left:-999px;"
alt=""/></noscript>

  <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-NWBSJ2F');</script>

<!-- End Google Tag Manager -->

  @yield('css')

  @yield('seo')

</head>
<body>
  <div class="wrapper">
    <!-- mob-menu -->
    <div class="mob-menu">
      <div class="mob-menu__inner">
        <div class="mob-menu__body">
          <nav class="mob-menu-nav">
            <ul>
              <li><a href="/about">О комплексе</a></li>
              <li>
                <div class="mob-acco js-acco">
                  <div class="mob-acco__item js-acco-item">
                    <div class="mob-acco__item-head js-acco-head">
                      <span class="mob-acco__item-title">Для кого</span>
                    </div>
                    <div class="mob-acco__item-body js-acco-body">
                      <div class="mob-acco__item-content">
                        <ul>
                          <li><a href="/hochu-stat-mamoj-5-kriteriev-uspekha">Планирование беременности</a></li>
                          <li><a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti">Беременность</a></li>
                          <li><a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha"> Кормление малыша</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li>
                <div class="mob-acco js-acco">
                  <div class="mob-acco__item js-acco-item">
                    <div class="mob-acco__item-head js-acco-head">
                      <span class="mob-acco__item-title">Калькуляторы</span>
                    </div>
                    <div class="mob-acco__item-body js-acco-body">
                      <div class="mob-acco__item-content">
                        <ul>
                          <li><a href="/become-mom-calculator">Калькулятор овуляции</a></li>
                          <li><a href="/calculator_pregnancy">Калькулятор беременности</a></li>
                          <li><a href="/calculator_height-weight">Калькулятор роста и веса</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li><a href="/articles">Полезные статьи</a></li>
              <li><a href="/reviews">Отзывы</a></li>
              <li><a href="/instruction">Инструкция</a></li>
              <li><a href="http://prof.vitagynal.ru/">Для специалистов</a></li>
            </ul>
          </nav>

          <a href="@if (!isset($ddd)){{ '/' }}@endif#pay" class="btn btn-fill-blue mob-menu-pay-btn scroll-to">Где купить</a>
        </div>

        <div class="mob-menu__disclamer">
          БИОЛОГИЧЕСКИ АКТИВНАЯ ДОБАВКА К ПИЩЕ. НЕ ЯВЛЯЕТСЯ ЛЕКАРСТВЕННЫМ СРЕДСТВОМ
        </div>
      </div>
    </div>
    <!-- /mob-menu -->
    <!-- header -->
    <header class="header">
      <div class="header__content">
        <a href="/" class="logo header-logo">
          <img src="/assets/img/icons/logo.svg" alt="">
        </a>

        <div class="header__actions">
          <!-- dropdown--open: toggle class -->
          <div class="dropdown header-dropdown js-dropdown">
            <div class="dropdown__text js-toggle-dropdown">Для кого</div>

            <div class="dropdown__popup">
              <div class="dropdown__popup-inner">
               <ul>
                  <li>
                    <a href="/hochu-stat-mamoj-5-kriteriev-uspekha">
                      <img src="/assets/img/icons/drop-down-ico-1.svg" alt="">
                      Планирование беременности
                    </a>
                  </li>
                  <li>
                    <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti">
                      <img src="/assets/img/icons/drop-down-ico-2.svg" alt="">
                      Беременность
                    </a>
                  </li>
                  <li>
                    <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha">
                      <img src="/assets/img/icons/drop-down-ico-3.svg" alt="">
                      Кормление малыша
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <nav class="header-nav">
            <ul>
              <li><a href="/about">О комплексе</a></li>
              <li>
                <span class="nav-dropdown">
                  <span class="nav-dropdown__label" tabindex="0">Калькуляторы</span>
                  <span class="nav-dropdown__popup">
                    <ul>
                      <li><a href="/become-mom-calculator">Калькулятор овуляции</a></li>
                      <li><a href="/calculator_pregnancy">Калькулятор беременности</a></li>
                      <li><a href="/calculator_height-weight">Калькулятор роста и веса</a></li>
                    </ul>
                  </span>
                </span>
              </li>
              <li><a href="/articles">Полезные статьи</a></li>
              <li><a href="/reviews">Отзывы</a></li>
              <li><a href="/instruction">Инструкция</a></li>
              <li><a href="http://prof.vitagynal.ru/">Для специалистов</a></li>
              
            </ul>
          </nav>

          <a href="@if (!isset($ddd)){{ '/' }}@endif#pay" class="btn btn-fill-blue header-pay-btn js-scroll-to">
            <span class="btn__inner">
              <span class="btn__text">Где купить</span>
            </span>
          </a>

          <button class="mob-menu-btn js-mob-menu-btn">
            <span class="mob-menu-btn__inner">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </button>
        </div>

      </div>
    </header>

    @yield('content')
    <!-- /main -->

     <footer class="footer pink-wrapper">
      <div class="container">
        <div class="footer__top">
          <div class="footer__row">
            <div class="footer__top-col">
              <a href="/" class="logo footer-logo">
                <img src="/assets/img/icons/logo.svg" alt="">
              </a>
              <!-- <div class="socials">
                <ul>
                  <li>
                    <a href="https://www.instagram.com/vitaginal/">
                      <img src="/assets/img/icons/inst.svg" alt="">
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/Vitaginal">
                      <img src="/assets/img/icons/fb.svg" alt="">
                    </a>
                  </li>
                </ul>
              </div> -->
            </div>
            <div class="footer__top-col">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="/about"><u>О препарате</u></a>
                </li>
                <li>
                  <a href="/articles"><u>Полезные статьи</u></a>
                </li>
                <li>
                  <a href="/reviews"><u>Отзывы</u></a>
                </li>
                <li>
                  <a href="/vitazhinal-instrukcziya.pdf"><u>Инструкция</u></a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="footer__top-col">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="/hochu-stat-mamoj-5-kriteriev-uspekha"><u>Хочу стать мамой</u></a>
                </li>
                <li>
                  <a href="/zhdem-malysha-5-prichin-schastlivoj-beremennosti"><u>Ждем малыша</u></a>
                </li>
                <li>
                  <a href="/mama-ya-rodilsya-5-sostavlyayushchih-zdorovya-malysha"><u>Мама, я родился</u></a>
                </li>
              </ul>
            </nav>
          </div>
            <div class="footer__top-col">
              <div class="footer__column-between">
                <nav class="footer-nav">
                  <ul>
                    <li>
                      <a href="/agreement">
                        <u>
                          Политика обработки и защиты персональных данных
                        </u>
                      </a>
                    </li>
                  </ul>
                </nav>

                <div class="footer-cookie">
                  Мы используем cookie! <a href="/agreement#cookie">Что это значит?</a>
                </div>
              </div>
            </div>
          </div>
          <button type="button" class="footer__scroll-top"></button>
        </div>
        <div class="footer__middle">
          <div class="footer__row">
            <div class="footer__middle-col">
              <a href="https://xn--90aiah5a.xn--p1ai/" class="logo footer-logo2">
                <img src="/assets/img/icons/logo_2.png" alt="">
              </a>
            </div>
            <div class="footer__middle-col">
              <div class="footer-text">
                "Безен Хелскеа" — производитель инновационных препаратов для мужского и женского здоровья
              </div>
            </div>
            <div class="footer__middle-col">
              <div class="footer-text">
                <address>
                  <p>
                    ООО "Безен Хелскеа РУС" Москва, 123022, ул. Сергея Макеева, 13 Бизнес-центр "Марр Плаза"
                  </p>
                </address>
              </div>
            </div>
            <div class="footer__middle-col">
              <div class="footer-contacts">
                <div class="footer-links">
                  <a href="mailto:info@besins-healthcare.com">info@besins-healthcare.com</a>
                  <a href="https://xn--90aiah5a.xn--p1ai/">Безен.рф</a>
                </div>
                <div class="footer-links">
                  <a href="tel:74959801067">T. +7(495) 980 1067</a>
                  <a href="tel:+74959801068">F. +7(495) 980 1068</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer__fixed-bottom">
        <div class="footer__сookie" style="display: none;">
          <div class="container">
            <p>Посещая этот сайт, вы разрешаете нам для его полноценного функционирования собирать ваши метаданные (cookie, IP-адрес и местоположение). Если вам нужна дополнительная информация или вы не хотите соглашаться с использованием cookies, пожалуйста, посетите страницу <a href="#">Пользовательское соглашение.</a> </p>
            <button type="button" class="btn btn-fill-white">
              <span class="btn__inner">
                <span class="btn__text">Согласна</span>
              </span>
            </button>
          </div>
        </div>
        <a href="@if (!isset($ddd)){{ '/' }}@endif#pay" class="footer__fixed-btn">Где купить</a>
        <div class="footer__disclamer">
          <div class="container">
            <img src="/assets/img/icons/disclamer.svg" alt="">
            <span>
              БИОЛОГИЧЕСКИ АКТИВНАЯ ДОБАВКА К ПИЩЕ. НЕ ЯВЛЯЕТСЯ ЛЕКАРСТВЕННЫМ СРЕДСТВОМ
            </span>
          </div>
        </div>
      </div>
    </footer>
    <!-- /footer -->
 </main>
    <!-- /main -->
  </div>

  <!-- Modal -->
  <div class="modal" id="" data-base-class="" data-slide-class="common-modal-class"></div>
  <!-- Modal -->

  <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  <script src="/assets/libs/object-fit-images_v3.2.4.js"></script>
  <script src="/assets/libs/gsap/gsap.min.js"></script>
  <script src="/assets/libs/gsap/ScrollTrigger.min.js"></script>
  <script src="/assets/libs/jquery-3.5.1.min.js"></script>
  <script src="/assets/libs/fancybox/jquery.fancybox.min.js"></script>
  <script src="/assets/libs/swiper/swiper.min.js"></script>
  <script src="/assets/libs/jquery.data-parallax.min.js"></script>
  <script src="/assets/js/main.js"></script>

  @yield('js')

  <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", https://mc.yandex.ru/metrika/tag.js, "ym"); ym(51170858, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src=https://mc.yandex.ru/watch/51170858 style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->


<script async src="//asset.fwcdn2.com/js/storyblock.js" type="text/javascript"></script>

</body>
</html>